package it.unibo.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTimeConverter {
    static HashMap<String, Integer> days = new HashMap<String, Integer>();
    static HashMap<String, Integer> months = new HashMap<String, Integer>();

    static {
        days.put("1", 1);
        days.put("01", 1);
        days.put("primo", 1);
        days.put("uno", 1);

        days.put("2", 2);
        days.put("02", 2);
        days.put("due", 2);

        days.put("3", 3);
        days.put("03", 3);
        days.put("tre", 3);

        days.put("4", 4);
        days.put("04", 4);
        days.put("quattro", 4);

        days.put("5", 5);
        days.put("05", 5);
        days.put("cinque", 5);

        days.put("6", 6);
        days.put("06", 6);
        days.put("sei", 6);

        days.put("7", 7);
        days.put("07", 7);
        days.put("sette", 7);

        days.put("8", 8);
        days.put("08", 8);
        days.put("otto", 8);

        days.put("9", 9);
        days.put("09", 9);
        days.put("nove", 9);

        days.put("10", 10);
        days.put("dieci", 10);

        days.put("11", 11);
        days.put("undici", 11);

        days.put("12", 12);
        days.put("dodici", 12);

        days.put("13", 13);
        days.put("tredici", 13);

        days.put("14", 14);
        days.put("quattordici", 14);

        days.put("15", 15);
        days.put("quindici", 15);

        days.put("16", 16);
        days.put("sedici", 16);

        days.put("17", 17);
        days.put("diciassette", 17);

        days.put("18", 18);
        days.put("diciotto", 18);

        days.put("19", 19);
        days.put("diciannove", 19);

        days.put("20", 20);
        days.put("venti", 20);

        days.put("21", 21);
        days.put("ventuno", 21);

        days.put("22", 22);
        days.put("ventidue", 22);

        days.put("23", 23);
        days.put("ventitre", 23);

        days.put("24", 24);
        days.put("ventiquattro", 24);

        days.put("25", 25);
        days.put("venticinque", 25);

        days.put("26", 26);
        days.put("ventisei", 26);

        days.put("27", 27);
        days.put("ventisette", 27);

        days.put("28", 28);
        days.put("ventotto", 28);

        days.put("29", 29);
        days.put("ventinove", 29);

        days.put("30", 30);
        days.put("trenta", 30);

        days.put("31", 31);
        days.put("trentuno", 31);

        months.put("1", 1);
        months.put("01", 1);
        months.put("gennaio", 1);
        months.put("gen", 1);

        months.put("2", 2);
        months.put("02", 2);
        months.put("febbraio", 2);
        months.put("feb", 2);

        months.put("3", 3);
        months.put("03", 3);
        months.put("marzo", 3);
        months.put("mar", 3);

        months.put("4", 4);
        months.put("04", 4);
        months.put("aprile", 4);
        months.put("apr", 4);

        months.put("5", 5);
        months.put("05", 5);
        months.put("maggio", 5);
        months.put("mag", 5);

        months.put("6", 6);
        months.put("06", 6);
        months.put("giugno", 6);
        months.put("giu", 6);

        months.put("7", 7);
        months.put("07", 7);
        months.put("luglio", 7);
        months.put("lug", 7);

        months.put("8", 8);
        months.put("08", 8);
        months.put("agosto", 8);
        months.put("ago", 8);

        months.put("9", 9);
        months.put("09", 9);
        months.put("settembre", 9);
        months.put("set", 9);

        months.put("10", 10);
        months.put("ottobre", 10);
        months.put("ott", 10);

        months.put("11", 11);
        months.put("novembre", 11);
        months.put("nov", 11);

        months.put("12", 12);
        months.put("dicembre", 12);
        months.put("dic", 12);
    }

    public static HashMap<String, Integer> getDays() {
        return days;
    }

    public static HashMap<String, Integer> getMonths() {
        return months;
    }

    public static LocalDateTime convert(String dateTimeString) {
        // TODO: Check italian accents
        Matcher matcher = Pattern.compile("[0-9a-zA-Z]*[^ ]").matcher(dateTimeString);
        ArrayList<String> resultList = new ArrayList<>();
        for (Object result : matcher.results().toArray()) {
            resultList.add(((MatchResult)result).group());
        }
        String day = resultList.get(0);
        String month = resultList.get(1);
        String year = resultList.get(2);
        LocalDate date = LocalDate.of(Integer.parseInt(year), DateTimeConverter.getMonths().get(month), DateTimeConverter.getDays().get(day));
        LocalDateTime dateTime = date.atStartOfDay();
        // TODO: Remove if useless
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        //LocalDateTime dateTime = LocalDateTime.parse(convertedDateTimeString, formatter);
        return dateTime;
    }
}
