package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.interpretation.Interpretation;

/**
 * It represents a {@link BotMessage} for replying to the user how to take their pressure and their heart rate correctly.
 */
class PressureHelpMessage extends AbstractBotMessage {
//    private static final String BASE_CONTENT = "Ricordati di attendere qualche minuto in posizione seduta prima di effettuare "
//                                               + "le misurazioni. Evita di effettuare le misurazioni dopo: pasti, fumo di "
//                                               + "sigarette, consumo di alcolici, sforzi fisici o stress emotivi. Posiziona il "
//                                               + "bracciale uno o due centimetri sopra la piega del gomito. Durante le "
//                                               + "misurazioni resta in posizione seduta, comoda, con il braccio rilassato e "
//                                               + "appoggiato in modo che il bracciale si trovi all'altezza del cuore";
    private static final String BASE_CONTENT = "La pressione a domicilio andrebbe misurata una o due volte alla settimana e comunque ogni 7 giorni prima di ogni visita dal medico\n\n"
        + "Per le misurazioni meglio preferire i giorni di lavoro normali\n\n"
        + "Sarebbe ideale che quando si misura la pressione si facciano 2 misurazioni mattino e sera\n\n"
        + "Misurare la pressione dopo 5 minuti di riposo in posizione seduta e mantenendo 1 minuto di distanza tra le misurazioni\n\n"
        + "Meglio misurare la pressione prima dell'assunzione di farmaci, se in trattamento\n\n"
        + "Scoraggiare il monitoraggio della pressione troppo frequente (per esempio ogni giorno)\n\n"
        + "Evitare autoregolazione del dosaggio dei farmaci in base alle automisurazioni\n\n";

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     */
    protected PressureHelpMessage(final TelegramBot bot) {
        super(bot, BASE_CONTENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        this.sendDefaultMessage(userMessage);
    }
}
