package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.interpretation.Interpretation;

import java.sql.SQLException;
import java.util.Objects;

/**
 * It represents a {@link BotMessage} for replying to the user their weekly average pressure and their weekly average heart
 * rate.
 */
class WeeklyMeanMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "La tua pressione media settimanale e' di %s/%s mmHg%s";
    private static final String MEAN_HR_CONTENT = " mentre la frequenza cardiaca media settimanale e' %s bpm";
    private static final String NO_MEAN_HR_CONTENT = "";
    private static final String NO_MEAN_CONTENT = "Non sono riuscito a recuperare la tua pressione media settimanale";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected WeeklyMeanMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) throws SQLException {
        this.db.getWeeklyMean(userMessage.getUserId(), o -> {
            o.ifPresentOrElse(m -> this.sendDefaultMessage(userMessage,
                                                           String.valueOf(m.getSystolicPressure()),
                                                           String.valueOf(m.getDiastolicPressure()),
                                                           m.getHeartRate()
                                                            .map(h -> String.format(MEAN_HR_CONTENT, h))
                                                            .orElse(NO_MEAN_HR_CONTENT)),
                              () -> this.sendMessage(userMessage, NO_MEAN_CONTENT));
        });
    }
}
