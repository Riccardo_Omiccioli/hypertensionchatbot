package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.db.Measurement;
import it.unibo.interpretation.Interpretation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * It represents a {@link BotMessage} for replying to the user pressure and heart rate measurements of today sent to the application.
 */
class GetPastWeekPressuresMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Tutte le misurazioni che mi hai fatto registrare questa settimana sono:\n%s";
    private static final String MEASUREMENT_LINE = "- %s/%s mmHg %s";
    private static final String DATETIME_PATTERN = "dd'/'LL'/'yyyy HH':'mm 'GMT'";
    private static final String MEASUREMENT_LINES_JOINER = "\n";
    private static final String ERROR_CONTENT = "Non ho nessuna misura inserita questa settimana";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected GetPastWeekPressuresMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        try {
            this.db.getUserMeasurements(
                    userMessage.getUserId(),
                    l -> Optional.of(l).filter(ms -> ms.size() > 0).ifPresentOrElse(ms -> {
                                this.sendDefaultMessage(userMessage,
                                        ms.parallelStream()
                                                .map(m -> String.format(MEASUREMENT_LINE,
                                                        m.getSystolicPressure(),
                                                        m.getDiastolicPressure(),
                                                        m.getTimestamp()
                                                                .format(DateTimeFormatter.ofPattern(DATETIME_PATTERN))))
                                                .collect(Collectors.joining(MEASUREMENT_LINES_JOINER)));
                            },
                            () -> this.sendMessage(userMessage, ERROR_CONTENT)), LocalDate.now().minusWeeks(1).atStartOfDay().plusDays(1), LocalDateTime.now());
        } catch (final Exception e) {
            this.sendMessage(userMessage, ERROR_CONTENT);
        }
    }

    /*
     * It creates a new list of measurements properties from a measurements list.
     */
    private <T> List<T> getListFromMeasurements(final List<? extends Measurement> measurements,
                                                final Function<Measurement, T> mapper) {
        return measurements.parallelStream().map(mapper::apply).collect(Collectors.toList());
    }
}
