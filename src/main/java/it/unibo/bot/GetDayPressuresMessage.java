package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.db.Measurement;
import it.unibo.interpretation.Interpretation;
import it.unibo.utilities.DateTimeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * It represents a {@link BotMessage} for replying to the user pressure and heart rate measurements of one day previously sent to the application.
 */
class GetDayPressuresMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Tutte le misurazioni che mi hai fatto registrare sono:\n%s";
    private static final String MEASUREMENT_LINE = "- %s/%s mmHg %s";
    private static final String DATETIME_PATTERN = "HH':'mm 'GMT'";
    private static final String MEASUREMENT_LINES_JOINER = "\n";
    private static final String ERROR_CONTENT = "Non ho nessuna misura per il giorno scelto";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected GetDayPressuresMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        try {
            String message = Objects.requireNonNull(userMessage.getText()).get();
            Matcher matcher = Pattern.compile("[0-9]{1,2} \\w* [0-9]{4}").matcher(message);
            LocalDateTime dateTime = null;
            if (matcher.find()) {
                String dateTimeString = matcher.group();
                dateTime = DateTimeConverter.convert(dateTimeString);
            }
            this.db.getUserMeasurements(
                    userMessage.getUserId(),
                    l -> Optional.of(l).filter(ms -> ms.size() > 0).ifPresentOrElse(ms -> {
                                this.sendDefaultMessage(userMessage,
                                        ms.parallelStream()
                                                .map(m -> String.format(MEASUREMENT_LINE,
                                                        m.getSystolicPressure(),
                                                        m.getDiastolicPressure(),
                                                        m.getTimestamp()
                                                                .format(DateTimeFormatter.ofPattern(DATETIME_PATTERN))))
                                                .collect(Collectors.joining(MEASUREMENT_LINES_JOINER)));
                            },
                            () -> this.sendMessage(userMessage, ERROR_CONTENT)), dateTime, dateTime.plusHours(24));
        } catch (final Exception e) {
            this.sendMessage(userMessage, ERROR_CONTENT);
        }
    }

    /*
     * It creates a new list of measurements properties from a measurements list.
     */
    private <T> List<T> getListFromMeasurements(final List<? extends Measurement> measurements,
                                                final Function<Measurement, T> mapper) {
        return measurements.parallelStream().map(mapper::apply).collect(Collectors.toList());
    }
}
