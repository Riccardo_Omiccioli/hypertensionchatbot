package it.unibo.bot;

import it.unibo.interpretation.Interpretation;

import java.sql.SQLException;

/**
 * It represents a message that can be sent by this application to the user in response to another message that the user itself
 * has previously sent to this application.
 */
interface BotMessage {
    /**
     * It sends this message to the user. It needs the {@link UserMessage} to which this message is sent in response to and the
     * {@link Interpretation} of the given {@link UserMessage}, so as to get all useful data to perform the "reply" operation.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param interpretation the {@link Interpretation} of the given {@link UserMessage}
     */
    void send(UserMessage userMessage, Interpretation interpretation) throws SQLException;
}
