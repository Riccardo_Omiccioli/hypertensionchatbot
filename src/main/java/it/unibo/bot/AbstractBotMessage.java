package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SendPhoto;

import java.util.Objects;

/**
 * An abstract implementation of {@link BotMessage} for factoring together the common elements of behavior between all concrete
 * implementations of {@link BotMessage}.
 */
abstract class AbstractBotMessage implements BotMessage {
    private final TelegramBot bot;
    private final String baseMessageContent;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the answer which is used as
     * a base for creating all possible variants of message which belong to this class. This answer can contain string format
     * characters to be substituted with the real values once the message is about to get sent.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param baseMessageContent the answer which is used as a base for creating all possible variants of message
     */
    protected AbstractBotMessage(final TelegramBot bot, final String baseMessageContent) {
        this.bot = Objects.requireNonNull(bot);
        this.baseMessageContent = Objects.requireNonNull(baseMessageContent);
    }

    /**
     * It sends this message in response to the given {@link UserMessage} and it will contain the base content provided in the
     * constructor formatted using the specified parameters.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param parameters the parameters used to format the base content provided in the constructor
     */
    protected void sendDefaultMessage(final UserMessage userMessage, final String... parameters) {
        this.bot.execute(new SendMessage(userMessage.getUserId(), String.format(this.baseMessageContent, (Object[]) parameters)));
    }

    /**
     * It sends this message in response to the given {@link UserMessage} and it will contain the base content provided in the
     * constructor formatted using the specified parameters. This message will also have the given reply mark-up.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param replyMarkup the reply mark-up this message will have
     * @param parameters the parameters used to format the base content provided in the constructor
     */
    protected void sendDefaultMessage(final UserMessage userMessage, final Keyboard replyMarkup, final String... parameters) {
        this.bot.execute(new SendMessage(userMessage.getUserId(), String.format(this.baseMessageContent, (Object[]) parameters))
                             .replyMarkup(replyMarkup));
    }

    /**
     * It sends a reply message in response to the given {@link UserMessage}. This message will also have the given reply mark-up.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param replyMessage the reply message to send to the user
     * @param replyMarkup the reply mark-up this message will have
     */
    protected void sendKeyboardMessage(final UserMessage userMessage, final String replyMessage, final Keyboard replyMarkup) {
        this.bot.execute(new SendMessage(userMessage.getUserId(), replyMessage).replyMarkup(replyMarkup));
    }

    /**
     * It sends this message in response to the given {@link UserMessage} and it will contain the content provided formatted using
     * the specified parameters.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param messageContent the content of this message
     * @param parameters the parameters used to format the given message content
     */
    protected void sendMessage(final UserMessage userMessage, final String messageContent, final String... parameters) {
        this.bot.execute(new SendMessage(userMessage.getUserId(), String.format(messageContent, (Object[]) parameters)));
    }

    /**
     * It sends this message in response to the given {@link UserMessage} and it will contain the content provided formatted using
     * the specified parameters. This message will also have the given reply mark-up.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param messageContent the content of this message
     * @param replyMarkup the reply mark-up this message will have
     * @param parameters the parameters used to format the given message content
     */
    protected void sendMessage(final UserMessage userMessage,
                               final String messageContent,
                               final Keyboard replyMarkup,
                               final String... parameters) {
        this.bot.execute(new SendMessage(userMessage.getUserId(), String.format(messageContent, (Object[]) parameters))
                             .replyMarkup(replyMarkup));
    }

    /**
     * It sends this message in response to the given {@link UserMessage} and it will contain the image content provided and the
     * image will have the given caption.
     * @param userMessage the {@link UserMessage} to which this message is sent in response to
     * @param messageContent the image content of this message
     * @param caption the caption of the image
     */
    protected void sendMessage(final UserMessage userMessage, final byte[] messageContent, final String caption) {
        this.bot.execute(new SendPhoto(userMessage.getUserId(), messageContent)
                             .caption(caption));
    }
}
