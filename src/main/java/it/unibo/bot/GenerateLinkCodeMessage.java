package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.interpretation.Interpretation;

import java.sql.SQLException;
import java.util.Objects;

/**
 * It represents a {@link BotMessage} for replying to the user their linkCode.
 */
class GenerateLinkCodeMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Il tuo link code e' %s\nRegistrati o effettua il login su hypertension.website e inserisci il codice nella pagina delle impostazioni account";
    private static final String ERROR_CONTENT = "Non sono riuscito a generate il tuo link code";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected GenerateLinkCodeMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        try {
            this.db.generateLinkCode(userMessage.getUserId(), o -> {
                o.ifPresentOrElse(m -> this.sendDefaultMessage(userMessage, o.get()),
                                  () -> this.sendMessage(userMessage, ERROR_CONTENT));
            });
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
