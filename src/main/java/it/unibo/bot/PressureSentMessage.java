package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import it.unibo.db.DBManager;
import it.unibo.db.MeasurementImpl;
import it.unibo.interpretation.Interpretation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * It represents a {@link BotMessage} for telling the user to have received their systolic and diastolic pressure measurement and
 * possibly their heart rate measurement. It then asks for the correctness of the extracted values and if that is the case, it
 * persists them, otherwise it discards them.
 */
class PressureSentMessage extends AbstractBotMessage implements BotMessageWithConfirmation {
    private static final int HIGH_SYSTOLIC_THRESHOLD = 135;
    private static final int HIGH_DIASTOLIC_THRESHOLD = 85;
    private static final String BASE_CONTENT = "Archiviero' questo risultato: pressione %s/%s mmHg%s. E' corretto?";
    private static final String HR_CONTENT = ", frequenza cardiaca %s bpm";
    private static final String NO_HR_CONTENT = "";
    private static final String CONFIRM_MEASUREMENT = "Si";
    private static final String DENY_MEASUREMENT = "No";
    private static final String BASE_CONFIRMATION_CONTENT = "Va bene, e' stato archiviato";
    private static final String HIGH_PRESSURE_CONTENT = ". Il valore era un po' alto, prova a contattare il tuo dottore.";
    private static final String SPIKE_PRESSURE_CONTENT = ". Un valore fuori norma ogni tanto non e' pericoloso.";
    private static final String NORMAL_PRESSURE_CONTENT = ". La tua pressione e' sotto controllo.";
    private static final String VALLEY_PRESSURE_CONTENT = "";
    private static final String NO_PREVIOUS_CONTENT = ". Non mi ricordo di nessuna precedente misurazione. Se vuoi puoi farne "
                                                      + "un'altra";
    private static final String DENY_CONFIMATION_CONTENT = "Va bene, non archiviero' il risultato";
    private static final String NOT_UNDERSTOOD_CONFIRMATION_CONTENT = "Scusami, non ho compreso la tua risposta";
    private static final String VALUES_MISUNDERSTOOD = "Penso che tu volessi inviarmi una misurazione di pressione, ma non ho "
                                                       + "capito i valori che mi hai detto. Prova di nuovo usando questo "
                                                       + "formato: \"massima [valore], minima [valore]\". Se "
                                                       + "conosci anche il valore della tua frequenza cardiaca, prova invece il "
                                                       + "formato: \"massima [valore], minima [valore], "
                                                       + "frequenza [valore]\"";
    private static final String ERROR_CONTENT = "Non sono riuscito a capire i valori";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected PressureSentMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        try {
            String message = Objects.requireNonNull(userMessage.getText()).get();
            ArrayList<Integer> allMatches = new ArrayList<>();
            Matcher matcher = Pattern.compile("[0-9]{2,3}").matcher(message);
            while (matcher.find()) {
                allMatches.add(Integer.valueOf(matcher.group()));
            }
            Integer systolic;
            Integer diastolic;
            Integer heartRate = null;
            if (allMatches.get(0) > allMatches.get(1)) {
                systolic = allMatches.get(0);
                diastolic = allMatches.get(1);
            } else {
                systolic = allMatches.get(1);
                diastolic = allMatches.get(0);
            }
            if (allMatches.toArray().length == 2) {
                heartRate = null;
            } else if (allMatches.toArray().length == 3) {
                heartRate = allMatches.get(2);
            }
            this.sendDefaultMessage(userMessage,
                    new ReplyKeyboardMarkup(new String[][] { new String[] { CONFIRM_MEASUREMENT },
                            new String[] { DENY_MEASUREMENT }},
                            false,
                            true,
                            false),
                    String.valueOf(systolic),
                    String.valueOf(diastolic),
                    heartRate == null ? NO_HR_CONTENT : String.format(HR_CONTENT, heartRate));
            interpretation.getParameters().put("systolic", Optional.ofNullable(systolic));
            interpretation.getParameters().put("diastolic", Optional.ofNullable(diastolic));
            interpretation.getParameters().put("heartRate", Optional.ofNullable(heartRate));
        } catch (final Exception e) {
            this.sendMessage(userMessage, VALUES_MISUNDERSTOOD);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendToConfirmation(final UserMessage userMessage, final Interpretation originalInterpretation, final ConfirmationAnswer answer) throws SQLException {
        switch (answer) {
            case YES:
                final var originalSystolic = originalInterpretation.getSystolicPressure().get();
                final var originalDiastolic = originalInterpretation.getDiastolicPressure().get();
                this.db.getMonthlyMean(userMessage.getUserId(), o -> this.sendMessage(
                    userMessage,
                    BASE_CONFIRMATION_CONTENT + o.map(m -> {
                        if ((originalSystolic > HIGH_SYSTOLIC_THRESHOLD || originalDiastolic > HIGH_DIASTOLIC_THRESHOLD)
                            && (m.getSystolicPressure() > HIGH_SYSTOLIC_THRESHOLD || m.getDiastolicPressure() > HIGH_DIASTOLIC_THRESHOLD)) {
                                return HIGH_PRESSURE_CONTENT + " La tua pressione media mensile e' di " + m.getSystolicPressure() + "/" + m.getDiastolicPressure() + "mmHg.";
                        } else if (originalSystolic > HIGH_SYSTOLIC_THRESHOLD || originalDiastolic > HIGH_DIASTOLIC_THRESHOLD) {
                            return SPIKE_PRESSURE_CONTENT + " La tua pressione media mensile e' di " + m.getSystolicPressure() + "/" + m.getDiastolicPressure() + "mmHg.";
                        } else if (m.getSystolicPressure() <= HIGH_SYSTOLIC_THRESHOLD && m.getDiastolicPressure() <= HIGH_DIASTOLIC_THRESHOLD) {
                            return NORMAL_PRESSURE_CONTENT + " La tua pressione media mensile e' di " + m.getSystolicPressure() + "/" + m.getDiastolicPressure() + "mmHg.";
                        }
                        return VALLEY_PRESSURE_CONTENT;
                    }).orElse(NO_PREVIOUS_CONTENT),
                    new ReplyKeyboardRemove()));
                this.db.persistUserMeasurement(new MeasurementImpl(originalSystolic,
                                                                   originalDiastolic,
                                                                   originalInterpretation.getHeartRate(),
                                                                   userMessage.getTimestamp(),
                                                                   userMessage.getUserId()));
                break;
            case NO:
                this.sendMessage(userMessage, DENY_CONFIMATION_CONTENT, new ReplyKeyboardRemove());
                break;
            default:
                this.sendMessage(userMessage, NOT_UNDERSTOOD_CONFIRMATION_CONTENT);
                break;
        } 
    }
}
