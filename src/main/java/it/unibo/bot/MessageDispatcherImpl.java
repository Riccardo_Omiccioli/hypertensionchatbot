package it.unibo.bot;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.ImmutableClassToInstanceMap;
import com.pengrad.telegrambot.BotUtils;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import com.pengrad.telegrambot.request.SendMessage;
import it.unibo.app.Application;
import it.unibo.db.DBManager;
import it.unibo.interpretation.*;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An implementation of the {@link MessageDispatcher} interface.
 */
public class MessageDispatcherImpl implements MessageDispatcher {
    private static final String START_COMMAND = "/start";
    private static final String START_MESSAGE = "Ciao! Mi chiamo Hypertension Bot e sono il chatbot per aiutarti a "
                                              + "misurarti la pressione. Potrai farmi le seguenti richieste:\n\n"
                                              + "- Chiedere informazioni\n\n"
                                              + "- Inserire una nuova misura\n\n"
                                              + "- Richiedere la tua media giornaliera, settimanale o mensile\n\n"
                                              + "- Richiedere la tua media in un periodo di giorni a tua scelta\n\n"
                                              + "- Richiedere tutte le misure inserite nell'ultimo giorno, settimana o mese\n\n"
                                              + "- Richiedere tutte le misure inserite in un giorno a tua scelta\n\n"
                                              + "- Visualizzare tutte le misure inserite in un periodo di giorni a tua scelta\n\n"
                                              + "- Visualizzare tutte le misure inserite nel sistema";
    private static final String HELP_COMMAND = "/help";
    private static final String HELP_MESSAGE = "Le richieste che puoi farmi sono:\n\n"
                                             + "- Chiedere informazioni\n\n"
                                             + "- Inserire una nuova misura\n\n"
                                             + "- Richiedere la tua media giornaliera, settimanale o mensile\n\n"
                                             + "- Richiedere la tua media in un periodo di giorni a tua scelta\n\n"
                                             + "- Richiedere tutte le misure inserite nell'ultimo giorno, settimana o mese\n\n"
                                             + "- Richiedere tutte le misure inserite in un giorno a tua scelta\n\n"
                                             + "- Visualizzare tutte le misure inserite in un periodo di giorni a tua scelta\n\n"
                                             + "- Visualizzare tutte le misure inserite nel sistema";
    private static final String ID_COMMAND = "/id";
    private static final String CONSENT_COMMAND = "/Acconsento";
    private static final String CONSENT_DENY = "Non acconsento";
    private static final String CONSENT_APPROVE = "Acconsento";
    private static final String CONSENT_MESSAGE = "Prima di poter utilizzare il sistema devi fornire il tuo consenso "
                                                + "alla memorizzazione dei dati che inserirai. Per procedere utilizza il pulsante sottostante o scrivi ed invia \"acconsento\"";
    private static final String CONSENT_APPROVED_MESSAGE = "Consenso fornito con successo. Ora puoi inserire misure e ricevere notifiche.";
    private static final String CONSENT_DENIED_MESSAGE = "Non hai fornito il consento. Potrai fornirlo successivamente se vorrai utilizzare questo chatbot.";

    private static final String NOT_UNDERSTOOD_CONTENT = "Scusami, non penso di aver capito";
    private static final String GENERIC_ERROR_MESSAGE = "Qualcosa e' andato storto. Riprova";
    private static final String NOT_SUPPORTED_ERROR_MESSAGE = "Tipo di messaggio non supportato. Riprova";
    private static final String WAITED_ERROR_MESSAGE = "Sembra che qualcosa sia andato storto, se stavi inserendo una misura ricomincia da capo";

    private static final int THIRD_ALERT_DAYS = 9;
    private static final String THIRD_ALERT_CONTENT = "E' da piu' di nove giorni che non ti misuri la pressione. "
                                                      + "Oggi potrebbe essere un buon giorno per farlo";
    private static final int SECOND_ALERT_DAYS = 5;
    private static final String SECOND_ALERT_CONTENT = "E' da piu' di cinque giorni che non ti misuri la pressione. "
                                                       + "Oggi potrebbe essere un buon giorno per farlo";
    private static final int FIRST_ALERT_DAYS = 2;
    private static final String FIRST_ALERT_CONTENT = "E' da piu' di due giorni che non ti misuri la pressione. "
                                                      + "Oggi potrebbe essere un buon giorno per farlo";

    private static final int MEDICATION_ALERT_MONTHS = 1;
    private static final String MEDICATION_ALERT_CONTENT = "Ti sei ricordato di prendere la tua terapia oggi?";

    private static final String SYMBOLS_REGEX = "[^\\p{L}\\p{N}\\p{P}\\p{Z}]|[\\x{0001f300}-\\x{0001f64f}]|"
                                                + "[\\x{0001f680}-\\x{0001f6ff}]";
    private static final String SYMBOLS_REPLACEMENT = "";
    private static final String PRESSURE_REGEX = "([0-9]{2,3})\\/([0-9]{2})";
    private static final String PRESSURE_REPLACEMENT = "$1 / $2";

    private final Application app;
    private final TelegramBot bot;
    private final Interpreter nlInterpreter;
    private final DBManager db;
    private final Map<InterpretationType, Class<? extends BotMessage>> answersAssociations;
    private final ClassToInstanceMap<BotMessage> answers;
    private final Map<Integer, Interpretation> usersWaitedAnswer;
    private final Map<Integer, Integer> userWaitedCount;

    ReplyKeyboardMarkup consentKeyboard = new ReplyKeyboardMarkup(new String[][] {
            new String[] { CONSENT_APPROVE },
            new String[] { CONSENT_DENY }},
            false,
            true,
            false);

    /**
     * This constructor takes the object which represents the application as a whole, an interpreter for the natural language in
     * order to understand the content of text messages, an interpreter for file images in order to obtain systolic and diastolic
     * pressure values from images, a database object for querying previously persisted data and the token for the telegram bot.
     * @param app the object which represents the application as a whole
     * @param nlInterpreter the interpreter for the natural language of text messages content
     * @param db the database object for querying previously persisted data
     * @param botToken the token for the telegram bot
     */
    public MessageDispatcherImpl(final Application app,
                                 final NaturalLanguageInterpreter nlInterpreter,
                                 final DBManager db,
                                 final String botToken) throws Exception{
        this.app = Objects.requireNonNull(app);
        this.bot = new TelegramBot(botToken);
        this.nlInterpreter = Objects.requireNonNull(nlInterpreter);
        this.db = Objects.requireNonNull(db);
        this.answersAssociations = new EnumMap<>(InterpretationType.class);
        this.answersAssociations.put(InterpretationType.GET_ID, GetIdUserMessage.class);
        this.answersAssociations.put(InterpretationType.GENERATE_LINK_CODE, GenerateLinkCodeMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_INFO, PressureHelpMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_MEAN_BETWEEN, BetweenMeanMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_MEAN_DAY, DailyMeanMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_MEAN_WEEK, WeeklyMeanMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_MEAN_MONTH, MonthlyMeanMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_SENT, PressureSentMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_ALL, GetAllPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_BETWEEN, GetBetweenPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_DAY, GetDayPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_PAST_DAY, GetPastDayPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_PAST_WEEK, GetPastWeekPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.PRESSURE_GET_PAST_MONTH, GetPastMonthPressuresMessage.class);
        this.answersAssociations.put(InterpretationType.THANKS, ThanksMessage.class);
        this.answersAssociations.put(InterpretationType.HELLO, HelloMessage.class);
        this.answers = ImmutableClassToInstanceMap.<BotMessage>builder()
            .put(GetIdUserMessage.class, new GetIdUserMessage(this.bot, db))
            .put(GenerateLinkCodeMessage.class, new GenerateLinkCodeMessage(this.bot, db))
            .put(PressureHelpMessage.class, new PressureHelpMessage(this.bot))
            .put(BetweenMeanMessage.class, new BetweenMeanMessage(this.bot, db))
            .put(DailyMeanMessage.class, new DailyMeanMessage(this.bot, db))
            .put(WeeklyMeanMessage.class, new WeeklyMeanMessage(this.bot, db))
            .put(MonthlyMeanMessage.class, new MonthlyMeanMessage(this.bot, db))
            .put(PressureSentMessage.class, new PressureSentMessage(this.bot, db))
            .put(GetAllPressuresMessage.class, new GetAllPressuresMessage(this.bot, db))
            .put(GetBetweenPressuresMessage.class, new GetBetweenPressuresMessage(this.bot, db))
            .put(GetDayPressuresMessage.class, new GetDayPressuresMessage(this.bot, db))
            .put(GetPastDayPressuresMessage.class, new GetPastDayPressuresMessage(this.bot, db))
            .put(GetPastWeekPressuresMessage.class, new GetPastWeekPressuresMessage(this.bot, db))
            .put(GetPastMonthPressuresMessage.class, new GetPastMonthPressuresMessage(this.bot, db))
            .put(ThanksMessage.class, new ThanksMessage(this.bot))
            .put(HelloMessage.class, new HelloMessage(this.bot))
            .build();
        this.usersWaitedAnswer = new HashMap<>();
        this.handleTimer();
        this.app.startDailyTimer(this::handleTimer);
        this.userWaitedCount = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processMessage(final String message) {
        final var update = Optional.ofNullable(BotUtils.parseUpdate(message));
        update.map(Update::editedMessage)
              .or(() -> update.map(Update::message))
              .ifPresent(m -> {
                  System.out.println("--------------------------------------------------\nm >>> " + m.text());
                  final var id = m.from().id(); // Telegram user id
                  final var timestamp = Instant.ofEpochSecond(m.date()).atZone(ZoneId.systemDefault()).toLocalDateTime();
                  Optional.ofNullable(m.text()).ifPresentOrElse(t -> {
                      System.out.println("t >>> " + t);
                      if (t.equals(START_COMMAND)) {
                          this.bot.execute(new SendMessage(id, START_MESSAGE));
                          this.bot.execute(new SendMessage(id, CONSENT_MESSAGE).replyMarkup(consentKeyboard));
                      } else if (t.equals(HELP_COMMAND)) {
                          this.bot.execute(new SendMessage(id, HELP_MESSAGE));
                      } else if (t.equals(ID_COMMAND)) {
                          this.bot.execute(new SendMessage(id, id.toString()));
                      } else if (t.equals(CONSENT_COMMAND) || t.equalsIgnoreCase(CONSENT_APPROVE)) {
                          try { this.db.persistUser(m); } catch (SQLException exception) { exception.printStackTrace(); }
                          this.bot.execute(new SendMessage(id, CONSENT_APPROVED_MESSAGE).replyMarkup(new ReplyKeyboardRemove()));
                      } else if (t.equalsIgnoreCase(CONSENT_DENY)) {
                          this.bot.execute(new SendMessage(id, CONSENT_DENIED_MESSAGE));
                      } else {
                          final var escapedText = t.replaceAll(SYMBOLS_REGEX, SYMBOLS_REPLACEMENT);
                          // TODO: Check regex
                          final var replaced = Pattern.compile(PRESSURE_REGEX).matcher(escapedText).replaceAll(PRESSURE_REPLACEMENT);
                          final var userMessage = new UserMessageImpl(id, timestamp, replaced);
                          try {
                              if (this.db.checkUser(id)) {
                                  this.nlInterpreter.interpret(userMessage, i -> {
                                      System.out.println("i >>> " + i.toString());
                                      Optional.ofNullable(this.answersAssociations.get(i.getType())).ifPresentOrElse(
                                          c -> {
                                              System.out.println("c >>> " + c.toString() + "\n--------------------------------------------------");
                                              if (!this.usersWaitedAnswer.containsKey(id)) {
                                                  if (i.getType() == InterpretationType.PRESSURE_SENT) {
                                                      ArrayList<Integer> allMatches = new ArrayList<>();
                                                      Matcher matcher = Pattern.compile("[0-9]{2,3}").matcher(t);
                                                      while (matcher.find()) {
                                                          allMatches.add(Integer.valueOf(matcher.group()));
                                                      }
                                                      if (allMatches.toArray().length >= 2) {
                                                        this.usersWaitedAnswer.put(id, i);
                                                      }
                                                  }
                                                  // TODO: Sends messages according to interpretation type, check if possible to improve
                                                  try {
                                                      this.answers.getInstance(c).send(userMessage, i);
                                                  } catch (SQLException exception) {
                                                      exception.printStackTrace();
                                                  }
                                              } else {
                                                  try {
                                                      this.answers.getInstance(PressureSentMessage.class).sendToConfirmation(userMessage,
                                                                          new InterpretationImpl.Builder().build(), ConfirmationAnswer.NOT_UNDERSTOOD);
                                                  } catch (SQLException exception) {
                                                      exception.printStackTrace();
                                                  }
                                              }
                                          },
                                          () -> {
                                              if (this.usersWaitedAnswer.containsKey(id)) {
                                                  try {
                                                      this.answers.getInstance(PressureSentMessage.class).sendToConfirmation(userMessage, this.usersWaitedAnswer.get(id),
                                                              i.getType() == InterpretationType.AGREEMENT
                                                                      ? ConfirmationAnswer.YES
                                                                      : (i.getType() == InterpretationType.DISAGREEMENT
                                                                      ? ConfirmationAnswer.NO
                                                                      : ConfirmationAnswer.NOT_UNDERSTOOD));
                                                  } catch (SQLException exception) {
                                                      exception.printStackTrace();
                                                  }
                                                  if (i.getType() == InterpretationType.AGREEMENT || i.getType() == InterpretationType.DISAGREEMENT) {
                                                      this.usersWaitedAnswer.remove(id);
                                                      this.userWaitedCount.remove(id);
                                                  }
                                                  if (i.getType() == InterpretationType.AGREEMENT) {
                                                      try {
                                                          this.db.updateUserMeasurementAlerts(id, false, false, false);
                                                      } catch (SQLException exception) {
                                                          exception.printStackTrace();
                                                      }
                                                  }
                                                  // TODO: Fix pressure_post message lock
                                                  /* Checks if the user needs to send a waited answer and if the interpretation
                                                   * is not an AGREEMENT or a DISAGREEMENT for n times in a row the waited answer is removed
                                                   */
                                                  if(i.getType() != InterpretationType.AGREEMENT && i.getType() != InterpretationType.DISAGREEMENT) {
                                                      if(this.userWaitedCount.containsKey(id)) {
                                                          this.userWaitedCount.put(id, this.userWaitedCount.get(id) + 1);
                                                      } else {
                                                          this.userWaitedCount.put(id, 1);
                                                      }
                                                  }
                                                  if(this.userWaitedCount.containsKey(id)) {
                                                      if(this.userWaitedCount.get(id) >= 3) {
                                                          this.usersWaitedAnswer.remove(id);
                                                          this.userWaitedCount.remove(id);
                                                          this.bot.execute(new SendMessage(id, WAITED_ERROR_MESSAGE).replyMarkup(new ReplyKeyboardRemove()));
                                                      }
                                                  }
                                              } else {
                                                  this.bot.execute(new SendMessage(id, NOT_UNDERSTOOD_CONTENT));
                                              }
                                          });
                                  });
                              } else {
                                  this.bot.execute(new SendMessage(id, CONSENT_MESSAGE).replyMarkup(consentKeyboard));
                              }
                          } catch (SQLException exception) {
                              exception.printStackTrace();
                          }
                      }
                  }, () ->{
                      this.bot.execute(new SendMessage(id, NOT_SUPPORTED_ERROR_MESSAGE));
                  });
              });
    }

    /**
     * This is the callback routine to be called by the timer which periodically executes tasks once per day.
     */
    private void handleTimer() {
        try {
        this.db.getLastMeasurementDates(d -> d.parallelStream()
            .forEach(p -> {
                final var user = p.getLeft();
                final var lastMeasurementTime = p.getRight();
                final var userId = user.getId();
                // Third alert check
                if (lastMeasurementTime.isBefore(LocalDateTime.now().minusDays(THIRD_ALERT_DAYS)) && !user.hasReceivedThirdAlert()) {
                    this.bot.execute(new SendMessage(userId, THIRD_ALERT_CONTENT));
                    try {
                        this.db.updateUserMeasurementAlerts(userId, true, true, true);
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                    }
                // Second alert check
                } else if (lastMeasurementTime.isBefore(LocalDateTime.now().minusDays(SECOND_ALERT_DAYS)) && !user.hasReceivedSecondAlert()) {
                    this.bot.execute(new SendMessage(userId, SECOND_ALERT_CONTENT));
                    try {
                        this.db.updateUserMeasurementAlerts(userId, true, true, false);
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                    }
                // First alert check
                } else if (lastMeasurementTime.isBefore(LocalDateTime.now().minusDays(FIRST_ALERT_DAYS)) && !user.hasReceivedFirstAlert()) {
                    this.bot.execute(new SendMessage(userId, FIRST_ALERT_CONTENT));
                    try {
                        this.db.updateUserMeasurementAlerts(userId, true, false, false);
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                    }
                }
                // Medication alert date check
                if (user.getMedicationAlertDate().isBefore(LocalDate.now().minusMonths(MEDICATION_ALERT_MONTHS))) {
                    this.bot.execute(new SendMessage(userId, MEDICATION_ALERT_CONTENT));
                    try {
                        this.db.renewUserMedicationAlerts(user.getId());
                    } catch (SQLException exception) {
                        exception.printStackTrace();
                    }
                }}));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
