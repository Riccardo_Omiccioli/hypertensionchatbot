package it.unibo.bot;

/**
 * The answer given to a confirmation by the user into one of their messages.
 */
public enum ConfirmationAnswer {
    /**
     * It represents the case when the requested confirmation has been confirmed.
     */
    YES,
    /**
     * It represents the case when the requested confirmation has been denied.
     */
    NO,
    /**
     * It represents the case when the answer to the requested confirmation has not been understood.
     */
    NOT_UNDERSTOOD
}
