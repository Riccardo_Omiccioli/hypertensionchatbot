package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.interpretation.Interpretation;

/**
 * It represents a {@link BotMessage} to reply when an user thanks the bot.
 */
class ThanksMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Prego, sempre a tua disposizione!";

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     */
    protected ThanksMessage(final TelegramBot bot) {
        super(bot, BASE_CONTENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        this.sendDefaultMessage(userMessage);
    }
}
