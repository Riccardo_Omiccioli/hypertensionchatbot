package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.db.Measurement;
import it.unibo.interpretation.Interpretation;
import it.unibo.utilities.DateTimeConverter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * It represents a {@link BotMessage} for replying to the user their average pressure and average heart rate in the period specified.
 */
class BetweenMeanMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "La tua pressione media nell'intervallo indicato e' di %s/%s mmHg%s";
    private static final String MEAN_HR_CONTENT = " mentre la frequenza cardiaca media e' %s bpm";
    private static final String NO_MEAN_HR_CONTENT = "";
    private static final String NO_MEAN_CONTENT = "Non sono riuscito a recuperare la tua pressione media nell'intervallo indicato";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected BetweenMeanMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        try {
            String message = Objects.requireNonNull(userMessage.getText()).get();
            Matcher matcher = Pattern.compile("[0-9]{1,2} \\w* [0-9]{4}").matcher(message);
            LocalDateTime startDateTime = null;
            LocalDateTime endDateTime = null;
            if (matcher.find()) {
                String dateTimeString = matcher.group();
                startDateTime = DateTimeConverter.convert(dateTimeString);
            }
            if (matcher.find()) {
                String dateTimeString = matcher.group();
                endDateTime = DateTimeConverter.convert(dateTimeString);
            }
            this.db.getUserMean(
                userMessage.getUserId(), o -> {
                o.ifPresentOrElse(m -> this.sendDefaultMessage(userMessage,
                                String.valueOf(m.getSystolicPressure()),
                                String.valueOf(m.getDiastolicPressure()),
                                m.getHeartRate().map(h -> String.format(MEAN_HR_CONTENT, h)).orElse(NO_MEAN_HR_CONTENT)),
                                () -> this.sendMessage(userMessage, NO_MEAN_CONTENT));},
                startDateTime,
                endDateTime.plusHours(24)
            );
        } catch (final Exception e) {
            this.sendMessage(userMessage, NO_MEAN_CONTENT);
        }
    }

    /*
     * It creates a new list of measurements properties from a measurements list.
     */
    private <T> List<T> getListFromMeasurements(final List<? extends Measurement> measurements,
                                                final Function<Measurement, T> mapper) {
        return measurements.parallelStream().map(mapper::apply).collect(Collectors.toList());
    }
}
