package it.unibo.bot;

/**
 * The application component responsible for handling the incoming user messages and dispatching response messages to the user.
 */
public interface MessageDispatcher {
    /**
     * It processes the message given as a string and encoded in JSON format. This means that it will try to understand its
     * content and formulate the most adequate response to it, which it will then be sent to the user which sent the processed
     * message.
     * @param message the message to process
     */
    void processMessage(String message);
}
