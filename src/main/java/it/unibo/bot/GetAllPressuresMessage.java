package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.db.DBManager;
import it.unibo.db.Measurement;
import it.unibo.interpretation.Interpretation;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.CategorySeries.CategorySeriesRenderStyle;
import org.knowm.xchart.style.Styler.ChartTheme;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.io.IOException;
import java.sql.SQLException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * It represents a {@link BotMessage} for replying to the user all pressure and heart rate measurements previously sent to the
 * application.
 */
class GetAllPressuresMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Tutte le misurazioni che mi hai fatto registrare sono:\n%s";
    private static final String MEASUREMENT_LINE = "- %s/%s mmHg %s";
    private static final String DATETIME_PATTERN = "dd'/'LL'/'yyyy HH':'mm 'GMT'";
    private static final String MEASUREMENT_LINES_JOINER = "\n";
    private static final String NO_MEASUREMENT_LINES = "Non ho nessuna tua precedente misurazione";
    private static final int GRAPH_SIZE = 1200;
    private static final String GRAPH_Y_AXIS = "mmHg / bpm";
    private static final String GRAPH_DATE_PATTERN = "dd/MM/YYYY HH:mm";
    private static final int GRAPH_X_AXIS_ROTATION = 45;
    private static final String SYSTOLIC_SERIES_NAME = "Massima";
    private static final String DIASTOLIC_SERIES_NAME = "Minima";
    private static final String HR_SERIES_NAME = "Frequenza";
    private static final String AVG_SUFFIX = " media";
    private static final String NO_GRAPH_ERROR = "Non sono riuscito a generare il grafico per i tuoi valori. Riprova";
    private static final String GRAPH_CAPTION = "Questo e' il grafico che mostra tutti i tuoi valori";

    private final DBManager db;

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message and the database object for
     * querying previously persisted data.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     * @param db the database object for querying previously persisted data
     */
    protected GetAllPressuresMessage(final TelegramBot bot, final DBManager db) {
        super(bot, BASE_CONTENT);
        this.db = Objects.requireNonNull(db);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) throws SQLException {
        this.db.getAllUserMeasurements(
            userMessage.getUserId(),
            l -> Optional.of(l).filter(ms -> ms.size() > 0).ifPresentOrElse(ms -> {
                this.sendDefaultMessage(userMessage,
                                        ms.parallelStream()
                                          .map(m -> String.format(MEASUREMENT_LINE,
                                                                  String.valueOf(m.getSystolicPressure()),
                                                                  String.valueOf(m.getDiastolicPressure()),
                                                                  m.getTimestamp()
                                                                   .format(DateTimeFormatter.ofPattern(DATETIME_PATTERN))))
                                          .collect(Collectors.joining(MEASUREMENT_LINES_JOINER)));
                final var timestamps = this.getListFromMeasurements(ms, m -> Date.from(m.getTimestamp()
                                                                                        .atZone(ZoneId.systemDefault())
                                                                                        .toInstant()));
                final var systolics = this.getListFromMeasurements(ms, Measurement::getSystolicPressure);
                final var diastolics = this.getListFromMeasurements(ms, Measurement::getDiastolicPressure);
                final var heartRates = this.getListFromMeasurements(ms, m -> m.getHeartRate()
                                                                              .map(Double::valueOf)
                                                                              .orElse(Double.NaN));
                final var chart = new CategoryChartBuilder().height(GRAPH_SIZE)
                                                            .width(GRAPH_SIZE)
                                                            .yAxisTitle(GRAPH_Y_AXIS)
                                                            .theme(ChartTheme.Matlab)
                                                            .build();
                chart.getStyler()
                     .setOverlapped(true)
                     .setDatePattern(GRAPH_DATE_PATTERN)
                     .setXAxisLabelRotation(GRAPH_X_AXIS_ROTATION);
                this.addSeries(chart, SYSTOLIC_SERIES_NAME, timestamps, systolics);
                this.addSeries(chart, DIASTOLIC_SERIES_NAME, timestamps, diastolics);
                this.addSeries(chart, HR_SERIES_NAME, timestamps, heartRates);
                this.addSeries(chart,
                               SYSTOLIC_SERIES_NAME + AVG_SUFFIX,
                               timestamps, systolics,
                               ns -> ns.mapToInt(Integer::intValue).average().orElse(0));
                this.addSeries(chart,
                               DIASTOLIC_SERIES_NAME + AVG_SUFFIX,
                               timestamps,
                               diastolics,
                               ns -> ns.mapToInt(Integer::intValue).average().orElse(0));
                this.addSeries(chart,
                               HR_SERIES_NAME + AVG_SUFFIX,
                               timestamps,
                               heartRates,
                               ns -> ns.filter(h -> !Double.isNaN(h)).mapToDouble(Double::doubleValue).average().orElse(0));
                try {
                    this.sendMessage(userMessage, BitmapEncoder.getBitmapBytes(chart, BitmapFormat.PNG), GRAPH_CAPTION);
                } catch (final IOException ex) {
                    this.sendMessage(userMessage, NO_GRAPH_ERROR);
                }
            },
            () -> this.sendMessage(userMessage, NO_MEASUREMENT_LINES)));
    }

    /*
     * It creates a new list of measurements properties from a measurements list.
     */
    private <T> List<T> getListFromMeasurements(final List<? extends Measurement> measurements,
                                                final Function<Measurement, T> mapper) {
        return measurements.parallelStream().map(mapper::apply).collect(Collectors.toList());
    }

    /*
     * It adds a new series to a chart given the name and the pairs of values along the axes.
     */
    private <N extends Number> void addSeries(final CategoryChart chart,
                                              final String seriesName,
                                              final List<?> xValues,
                                              final List<N> yValues) {
        chart.addSeries(seriesName, xValues, yValues).setChartCategorySeriesRenderStyle(CategorySeriesRenderStyle.Line);
    }

    /*
     * It adds a new series to a chart with the y value constant given the name of the series, the pairs of values along the axes
     * and the function which transforms the y values in a constant value.
     */
    private <N extends Number> void addSeries(final CategoryChart chart,
                                              final String seriesName,
                                              final List<?> xValues,
                                              final List<N> yValues,
                                              final Function<Stream<N>, Double> mapper) {
        chart.addSeries(seriesName,
                        xValues,
                        Collections.nCopies(yValues.size(),
                                            mapper.apply(yValues.parallelStream())))
                                                  .setChartCategorySeriesRenderStyle(CategorySeriesRenderStyle.Line)
                                                  .setMarker(SeriesMarkers.NONE);
    }
}
