package it.unibo.bot;

import it.unibo.interpretation.Interpretation;

import java.sql.SQLException;

/**
 * A {@link BotMessage} which asks the user for confirmation. It represents a bot message which is in fact made by two messages:
 * the first is sent in response to a user message, as any other {@link BotMessage}, but contains a yes or no question for the
 * user or, more specifically, a phrase which the user is needed to confirm or deny. The second message is then sent in response
 * to the user response to the phrase specified in the first message sent by the application.
 */
interface BotMessageWithConfirmation extends BotMessage {
    /**
     * It sends the second reply to the user. It needs the {@link UserMessage} to which the second message is sent in response to
     * and the {@link Interpretation} of the given {@link UserMessage}, so as to get all useful data to perform the "reply"
     * operation, and the {@link ConfirmationAnswer} to the confirmation requested to the user in the previous message, so as to
     * behave and reply correctly.
     * @param message the {@link UserMessage} to which the second message is sent in response to
     * @param originalInterpretation the {@link Interpretation} to the original {@link UserMessage} received from the user
     * @param confirmationAnswer the {@link ConfirmationAnswer} of the confirmation requested to the user in the previous message
     */
    void sendToConfirmation(UserMessage message,
                            Interpretation originalInterpretation,
                            ConfirmationAnswer confirmationAnswer) throws SQLException;
}
