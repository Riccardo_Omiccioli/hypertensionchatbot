package it.unibo.bot;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

/**
 * An implementation of the {@link UserMessage} interface.
 */
class UserMessageImpl implements UserMessage {

    private final int userId;
    private final LocalDateTime timestamp;
    private final Optional<String> text;

    /**
     * This constructor takes the id of the user which sent this message, the timestamp of the instant in which the user sent
     * this message and the text content of the message itself.
     * @param userId the id of the user which sent this message
     * @param timestamp the timestamp of the instant in which the user sent this message
     * @param text the text content of the message
     */
    protected UserMessageImpl(final int userId, final LocalDateTime timestamp, final String text) {
        this.userId = userId;
        this.timestamp = Objects.requireNonNull(timestamp);
        this.text = Optional.of(text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getUserId() {
        return this.userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<String> getText() {
        return this.text;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.timestamp, this.userId, this.text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserMessageImpl)) {
            return false;
        }
        final var other = (UserMessageImpl) obj;
        return this.timestamp.equals(other.timestamp)
               && this.userId == other.userId
               && this.text.equals(other.text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "UserMessageImpl [userId=" + this.userId + ", timestamp=" + this.timestamp + ", text=" + this.text + "]";
    }
}
