package it.unibo.bot;

import com.pengrad.telegrambot.TelegramBot;
import it.unibo.interpretation.Interpretation;

/**
 * It represents a {@link BotMessage} to reply when an user greets the bot.
 */
class HelloMessage extends AbstractBotMessage {
    private static final String BASE_CONTENT = "Ciao, chiedimi pure di inserire una nuova misura o di farti vedere quelle gia' aggiunte";

    /**
     * This constructor takes the {@link TelegramBot} which is responsible to send this message.
     * @param bot the {@link TelegramBot} which is responsible to send this message
     */
    protected HelloMessage(final TelegramBot bot) {
        super(bot, BASE_CONTENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final UserMessage userMessage, final Interpretation interpretation) {
        this.sendDefaultMessage(userMessage);
    }
}
