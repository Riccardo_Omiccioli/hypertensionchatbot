package it.unibo.bot;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * A message sent by the user through the front-end of the application. It contains all informations about which user sent it,
 * when they sent it and what the message was about.
 */
public interface UserMessage {
    /**
     * It returns the user id of the user which sent this message.
     * @return the user id of the user which sent this message
     */
    int getUserId();

    /**
     * It returns the timestamp of the instant in which the user sent this message.
     * @return the timestamp of the instant in which the user sent this message
     */
    LocalDateTime getTimestamp();

    /**
     * It returns the text content of this user message, if present.
     * @return an {@link Optional} containing the text content of this user message, if present, an {@link Optional#empty()}
     * otherwise
     */
    Optional<String> getText();
}
