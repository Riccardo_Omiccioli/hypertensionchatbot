package it.unibo.db;

import com.pengrad.telegrambot.model.Message;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;

/**
 * An implementation of the {@link DBManager} interface.
 */
public class DBManagerImpl implements DBManager {
    private static final String JDBC_CONNECT_STRING = "jdbc:postgresql://%s:%d/%s";
    private static final int LAST_MEASUREMENT_INDEX = 5;
    private static final int AVG_SYSTOLIC_INDEX = 0;
    private static final int AVG_DIASTOLIC_INDEX = 1;
    private static final int AVG_HR_INDEX = 2;
    private static final int FIRST_MEDICATION_ALERT_DAYS = 15;

    private final Connection db;

    /**
     * This constructor takes the port of the host which contains the database behind which the database is located, the host
     * which contains the database used by this application, the name of the database used by this application, the name of the
     * user on behalf of which this application is accessing the database, the password for the user on behalf of which this
     * application is accessing the database.
     * @param port the port of the host which contains the database behind which the database is located
     * @param host the host which contains the database used by this application
     * @param databaseName the name of the database used by this application
     * @param username the name of the user on behalf of which this application is accessing the database
     * @param password the password for the user on behalf of which this application is accessing the database
     */
    public DBManagerImpl(final int port, final String host, final String databaseName, final String username, final String password) throws SQLException {
        this.db = DriverManager.getConnection(String.format("jdbc:postgresql://%s:%d/%s?user=%s&password=%s", host, port, databaseName, username, password ));
    }

    public String getIdUser(final int idTelegram) throws SQLException {
        String query = "SELECT \"idUser\" FROM users.\"User\" WHERE \"idTelegram\" = ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setInt(1, idTelegram);
        ResultSet result = preparedStatement.executeQuery();
        if (result.next() != false) {
            return result.getString("idUser");
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getIdUser(final int idTelegram, final Consumer<Optional<String>> handler) throws SQLException {
        String query = "SELECT \"idUser\" FROM users.\"User\" WHERE \"idTelegram\" = ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setInt(1, idTelegram);
        ResultSet result = preparedStatement.executeQuery();
        if (result.next() != false) {
            handler.accept(Optional.ofNullable(result.getString("idUser").replaceAll("(.{3})(?!$)", "$1-")));
        } else {
            handler.accept(null);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generateLinkCode(final int idTelegram, final Consumer<Optional<String>> handler) throws SQLException {
        int codeLength = 18;
        String chars = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder(codeLength);
        for (int i = 0; i < codeLength; i++) {
            stringBuilder.append(chars.charAt(random.nextInt(chars.length())));
        }
        String query = "UPDATE users.\"User\" SET \"linkCode\" = ? WHERE \"idTelegram\" = ?;";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, stringBuilder.toString());
        preparedStatement.setInt(2, idTelegram);
        preparedStatement.execute();
        handler.accept(Optional.ofNullable(stringBuilder.toString().replaceAll("(.{3})(?!$)", "$1-")));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkUser(final int idTelegram) throws SQLException {
        String query = "SELECT \"idUser\" FROM users.\"User\" WHERE \"idTelegram\" = ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setInt(1, idTelegram);
        ResultSet result = preparedStatement.executeQuery();
        return result.next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persistUser(final Message message) throws SQLException {
        if (!checkUser(message.from().id())) {
            // Generates a random string of idLength chars chosen from defined chars
            int idLength = 12;
            String chars = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ";
            Random random = new Random();
            StringBuilder stringBuilder = new StringBuilder(idLength);
            for (int i = 0; i < idLength; i++) {
                stringBuilder.append(chars.charAt(random.nextInt(chars.length())));
            }
//            String query = "UPDATE users.\"User\" SET \"idTelegram\" = ? WHERE \"idUser\" = (SELECT \"idUser\" FROM users.\"User\" WHERE \"idTelegram\" IS NULL LIMIT 1);";
            String query = "INSERT INTO users.\"User\" (\"idUser\", \"idTelegram\") VALUES(?, ?)";
            PreparedStatement preparedStatement = this.db.prepareStatement(query);
            preparedStatement.setString(1, stringBuilder.toString());
            preparedStatement.setInt(2, message.from().id());
            // TODO: Check if user insert in database was successful
            preparedStatement.execute();
            insertConsent(message);
            insertAlert(message);
        }
    }

    public void insertConsent(final Message message) throws SQLException {
        String query = "INSERT INTO users.\"Consent\"(\"message\", \"date\", \"User_idUser\") VALUES(?, ?, ?) ";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, message.text());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.ofEpochSecond(Long.parseUnsignedLong(message.date().toString()),0, ZoneOffset.UTC)));
        preparedStatement.setString(3, getIdUser(message.from().id()));
        preparedStatement.execute();
    }

    public void insertAlert(final Message message) throws SQLException {
        String query = "INSERT INTO users.\"Alert\"(\"User_idUser\") VALUES(?) ";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, getIdUser(message.from().id()));
        preparedStatement.execute();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUserMeasurementAlerts(final int idTelegram, final boolean alertedAfterTwoDays, final boolean alertedAfterFiveDays, final boolean alertedAfterNineDays) throws SQLException {
        String query = "UPDATE users.\"Alert\" SET \"firstAlertGiven\" = ?, \"secondAlertGiven\" = ?, \"thirdAlertGiven\" = ? WHERE \"User_idUser\" = ?;";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setBoolean(1, alertedAfterTwoDays);
        preparedStatement.setBoolean(2, alertedAfterFiveDays);
        preparedStatement.setBoolean(3, alertedAfterNineDays);
        preparedStatement.setString(4, getIdUser(idTelegram));
        preparedStatement.execute();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void renewUserMedicationAlerts(final int idTelegram) throws SQLException {
        String query = "UPDATE  users.\"Alert\" SET \"medicationAlertDate\" = ? WHERE \"idUser\" = ?;";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
        // TODO: remove if line above is working
//        preparedStatement.setTimestamp(1, Timestamp.from(Instant.now()));
        preparedStatement.setString(2, getIdUser(idTelegram));
        preparedStatement.execute();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getLastMeasurementDates(final Consumer<List<? extends Pair<? extends User, LocalDateTime>>> handler) throws SQLException {
        String query = "SELECT \"idTelegram\" FROM users.\"User\" WHERE \"idTelegram\" IS NOT NULL";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        ResultSet result = preparedStatement.executeQuery();
        List<ImmutablePair<? extends User, LocalDateTime>> measurementDatesList = new ArrayList<>();
        if (result.next() == false) {
            handler.accept(null);
        } else {
            do {
                getLastUserMeasurement(result.getInt("idTelegram"), (Optional<? extends Pair<? extends User, LocalDateTime>> e) -> {
                    if (e.isPresent()) {
                        Pair<? extends User, LocalDateTime> pair = e.get();
                        measurementDatesList.add(new ImmutablePair<> ( pair.getLeft(), pair.getRight()));
                    }
                });
            } while (result.next());
            handler.accept(measurementDatesList);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getLastUserMeasurement(final int idTelegram, final Consumer<Optional<? extends Pair<? extends User, LocalDateTime>>> handler) throws SQLException {
        String query = "SELECT MAX(\"datetime\"), \"m\".\"User_idUser\", \"a\".\"firstAlertGiven\", \"a\".\"secondAlertGiven\", \"a\".\"thirdAlertGiven\", \"a\".\"medicationAlertDate\" " +
                "FROM users.\"Measurement\" AS \"m\" INNER JOIN users.\"Alert\" AS \"a\" ON \"m\".\"User_idUser\" = \"a\".\"User_idUser\" " +
                "WHERE \"m\".\"User_idUser\" = ? GROUP BY \"m\".\"User_idUser\", \"a\".\"firstAlertGiven\", \"a\".\"secondAlertGiven\", \"a\".\"thirdAlertGiven\", \"a\".\"medicationAlertDate\"";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setInt(1, idTelegram);
        ResultSet result = preparedStatement.executeQuery();
        if (result.next() == false) {
            handler.accept(Optional.empty());
        } else {
            handler.accept(Optional.of(new ImmutablePair<>(new UserImpl(idTelegram, result.getBoolean("firstAlertGiven"), result.getBoolean("secondAlertGiven"), result.getBoolean("thirdAlertGiven"),
                    result.getTimestamp("medicationAlertDate").toLocalDateTime().toLocalDate()), result.getTimestamp("dateTime").toLocalDateTime())));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persistUserMeasurement(final Measurement measurement) throws SQLException {
        String query = "INSERT INTO  users.\"Measurement\"(\"systolic\", \"diastolic\", \"heartRate\"," +
                "\"dateTime\", \"User_idUser\") VALUES(?, ?, ?, ?, ?) ";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setInt(1, measurement.getSystolicPressure());
        preparedStatement.setInt(2, measurement.getDiastolicPressure());
        if (measurement.getHeartRate().isEmpty()) {
            preparedStatement.setObject(3, null);
        } else {
            preparedStatement.setInt(3, measurement.getHeartRate().get());
        }
        preparedStatement.setTimestamp(4, Timestamp.valueOf(measurement.getTimestamp()));
        preparedStatement.setString(5, getIdUser(measurement.getUserId()));
        preparedStatement.execute();
    }

    public void getPastDayMeasures(final int userId, final Consumer<List<? extends Measurement>> handler) throws SQLException {
        this.getUserMeasurements(userId, handler, LocalDate.now().atStartOfDay().minusMonths(1), LocalDate.now().atStartOfDay().plusHours(24));
    }

    public void getPastWeekMeasures(final int userId, final Consumer<List<? extends Measurement>> handler) throws SQLException {
        this.getUserMeasurements(userId, handler, LocalDate.now().atStartOfDay().minusMonths(1), LocalDate.now().atStartOfDay().plusHours(24));
    }

    public void getPastMonthMeasures(final int userId, final Consumer<List<? extends Measurement>> handler) throws SQLException {
        this.getUserMeasurements(userId, handler, LocalDate.now().atStartOfDay().minusMonths(1), LocalDate.now().atStartOfDay().plusHours(24));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getUserMeasurements(final int idTelegram, final Consumer<List<? extends Measurement>> handler, final LocalDateTime startTime, final LocalDateTime endTime) throws SQLException {
        System.out.println("--- getUserMeasurement: " + startTime + "    |    " + endTime);
        String query = "SELECT * FROM users.\"Measurement\" WHERE \"User_idUser\" = ? AND \"dateTime\" BETWEEN ? AND ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, getIdUser(idTelegram));
        preparedStatement.setTimestamp(2, Timestamp.valueOf(startTime));
        preparedStatement.setTimestamp(3, Timestamp.valueOf(endTime));
        ResultSet result = preparedStatement.executeQuery();
        List<Measurement> measurementList = new ArrayList<>();
        if (result.next() == false) {
            handler.accept(null);
        } else {
            do {
                measurementList.add(new MeasurementImpl(result.getInt("systolic"), result.getInt("diastolic"), Optional.of(result.getInt("heartRate")),
                        result.getTimestamp("dateTime").toLocalDateTime(), idTelegram));
            } while (result.next());
            handler.accept(measurementList);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getAllUserMeasurements(final int idTelegram, final Consumer<List<? extends Measurement>> handler) throws SQLException {
        String query = "SELECT * FROM users.\"Measurement\" WHERE \"User_idUser\" = ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, getIdUser(idTelegram));
        ResultSet result = preparedStatement.executeQuery();
        List<MeasurementImpl> measurementList = new ArrayList<>();
        if (result.next() == false) {
            handler.accept(null);
        } else {
            do {
                measurementList.add(new MeasurementImpl(result.getInt("systolic"), result.getInt("diastolic"), Optional.of(result.getInt("heartRate")),
                        result.getTimestamp("dateTime").toLocalDateTime(), idTelegram));
            } while (result.next());
            handler.accept(measurementList);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getDayUserMeasurements(final int idTelegram, final Consumer<List<? extends Measurement>> handler,  final LocalDateTime startTime,  final LocalDateTime endTime) throws SQLException {
        this.getUserMeasurements(idTelegram, handler, startTime, endTime);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getDailyMean(final int userId, final Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException {
        this.getUserMean(userId, handler, LocalDate.now().atStartOfDay(), LocalDate.now().atStartOfDay().plusHours(24));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getWeeklyMean(final int userId, final Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException {
        this.getUserMean(userId, handler, LocalDate.now().atStartOfDay().minusWeeks(1), LocalDate.now().atStartOfDay().plusHours(24));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getMonthlyMean(final int userId, final Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException {
        this.getUserMean(userId, handler, LocalDate.now().atStartOfDay().minusMonths(1), LocalDate.now().atStartOfDay().plusHours(24));
    }

    /**
     * {@inheritDoc}
     */
    public void getUserMean(final int idTelegram, final Consumer<Optional<? extends UndatedMeasurement>> handler, final LocalDateTime startTime, final LocalDateTime endTime) throws SQLException {
        String query = "SELECT AVG(\"systolic\") AS \"sAVG\", AVG(\"diastolic\") AS \"dAVG\", AVG(\"heartRate\") AS \"hAVG\" " +
                "FROM users.\"Measurement\" WHERE \"User_idUser\" = ? AND \"dateTime\" BETWEEN ? AND ?";
        PreparedStatement preparedStatement = this.db.prepareStatement(query);
        preparedStatement.setString(1, getIdUser(idTelegram));
        preparedStatement.setTimestamp(2, Timestamp.valueOf(startTime));
        preparedStatement.setTimestamp(3, Timestamp.valueOf(endTime));
        ResultSet result = preparedStatement.executeQuery();
        if (result.next() == false) {
            handler.accept(Optional.empty());
        } else {
            handler.accept(Optional.of(new UndatedMeasurementImpl(result.getInt("sAVG"), result.getInt("dAVG"),
                    result.getInt("hAVG") != 0 ? Optional.of(result.getInt("hAVG")) : Optional.empty())));
        }
    }
}
