package it.unibo.db;

import java.util.Objects;
import java.util.Optional;

/**
 * An implementation of the {@link UndatedMeasurement} interface.
 */
class UndatedMeasurementImpl implements UndatedMeasurement {
    private final int systolicPressure;
    private final int diastolicPressure;
    private final Optional<Integer> heartRate;

    /**
     * This constructor takes the systolic pressure for this measurement, the diastolic pressure for this measurement and the
     * heart rate for this measurement, if it was measured.
     * @param systolicPressure the systolic pressure for this measurement
     * @param diastolicPressure the diastolic pressure for this measurement
     * @param heartRate an {@link Optional} containing the heart rate for this measurement, if it was measured, an
     * {@link Optional#empty()} otherwise
     */
    UndatedMeasurementImpl(final int systolicPressure, final int diastolicPressure, final Optional<Integer> heartRate) {
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.heartRate = Objects.requireNonNull(heartRate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSystolicPressure() {
        return this.systolicPressure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDiastolicPressure() {
        return this.diastolicPressure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getHeartRate() {
        return this.heartRate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.diastolicPressure, this.systolicPressure, this.heartRate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UndatedMeasurementImpl)) {
            return false;
        }
        final var other = (UndatedMeasurementImpl) obj;
        return this.diastolicPressure == other.diastolicPressure
               && this.systolicPressure == other.systolicPressure
               && this.heartRate.equals(other.heartRate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "MeasurementImpl [systolicPressure=" + this.systolicPressure + ", diastolicPressure=" + this.diastolicPressure
               + ", heartRate=" + this.heartRate + "]";
    }
}
