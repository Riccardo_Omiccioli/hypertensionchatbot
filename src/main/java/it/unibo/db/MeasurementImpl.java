package it.unibo.db;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

/**
 * An implementation of the {@link Measurement} interface.
 */
public class MeasurementImpl extends UndatedMeasurementImpl implements Measurement {
    private final LocalDateTime timestamp;
    private final int userId;

    /**
     * This constructor takes the systolic pressure for this measurement, the diastolic pressure for this measurement and the
     * heart rate for this measurement, if it was measured, the timestamp of the instant in which this measurement was made and
     * the id of the user which made this measurement.
     * @param systolicPressure the systolic pressure for this measurement
     * @param diastolicPressure the diastolic pressure for this measurement
     * @param heartRate an {@link Optional} containing the heart rate for this measurement, if it was measured, an
     * {@link Optional#empty()} otherwise
     * @param timestamp the timestamp of the instant in which this measurement was made
     * @param userId the id of the user which made this measurement
     */
    public MeasurementImpl(final int systolicPressure,
                           final int diastolicPressure,
                           final Optional<Integer> heartRate,
                           final LocalDateTime timestamp,
                           final int userId) {
        super(systolicPressure, diastolicPressure, heartRate);
        this.timestamp = Objects.requireNonNull(timestamp);
        this.userId = userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getUserId() {
        return this.userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getDiastolicPressure(),
                            this.getSystolicPressure(),
                            this.getHeartRate(),
                            this.timestamp,
                            this.userId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof MeasurementImpl)) {
            return false;
        }
        final var other = (MeasurementImpl) obj;
        return this.userId == other.userId
               && this.timestamp.equals(other.timestamp)
               && this.getDiastolicPressure() == other.getDiastolicPressure()
               && this.getSystolicPressure() == other.getSystolicPressure()
               && this.getHeartRate().equals(other.getHeartRate());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "UserMeasurementImpl [timestamp=" + this.timestamp + ", userId=" + this.userId + ", systolicPressure="
               + this.getSystolicPressure() + ", diastolicPressure=" + this.getDiastolicPressure() + ", heartRate="
               + this.getHeartRate() + "]";
    }
}
