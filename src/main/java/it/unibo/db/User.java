package it.unibo.db;

import java.time.LocalDate;

/**
 * A user as seen by the database, capable of containing all data concerning itself which is stored into the database.
 */
public interface User {
    /**
     * It returns the user id as intended by this application.
     * @return the user id as intended by this application
     */
    int getId();

    /**
     * It returns whether or not this user has received the first of the three alerts about the last time they measured their
     * blood pressure.
     * @return whether or not this user has received the first of the three alerts about the last time they measured their blood
     * pressure
     */
    boolean hasReceivedFirstAlert();

    /**
     * It returns whether or not this user has received the second of the three alerts about the last time they measured their
     * blood pressure.
     * @return whether or not this user has received the second of the three alerts about the last time they measured their blood
     * pressure
     */
    boolean hasReceivedSecondAlert();

    /**
     * It returns whether or not this user has received the third of the three alerts about the last time they measured their
     * blood pressure.
     * @return whether or not this user has received the third of the three alerts about the last time they measured their blood
     * pressure
     */
    boolean hasReceivedThirdAlert();

    /**
     * It returns the date in which this user has received the last alert about the medication they should be taking.
     * @return the date in which this user has received the last alert about the medication they should be taking
     */
    LocalDate getMedicationAlertDate(); 
}
