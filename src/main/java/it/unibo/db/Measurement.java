package it.unibo.db;

import java.time.LocalDateTime;

/**
 * A measurement with all its information and also the date in which was made and the user which made it.
 */
public interface Measurement extends UndatedMeasurement {
    /**
     * It returns the instant in which this measurement was made.
     * @return the instant in which this measurement was made
     */
    LocalDateTime getTimestamp();

    /**
     * It returns the id of the user which made this measurement.
     * @return the id of the user which made this measurement
     */
    int getUserId();
}
