package it.unibo.db;

import java.util.Optional;

/**
 * A measurement with all its information without the date in which was made and the user which made it.
 */
public interface UndatedMeasurement {
    /**
     * It returns the systolic pressure for this measurement.
     * @return the systolic pressure for this measurement
     */
    int getSystolicPressure();

    /**
     * It returns the diastolic pressure for this measurement.
     * @return the diastolic pressure for this measurement
     */
    int getDiastolicPressure();

    /**
     * It returns the heart rate for this measurement, if it was measured.
     * @return an {@link Optional} contianing the heart rate pressure for this measurement, if it was measured, an
     * {@link Optional#empty()} otherwise
     */
    Optional<Integer> getHeartRate();
}
