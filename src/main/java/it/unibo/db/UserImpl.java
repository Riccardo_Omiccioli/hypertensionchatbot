package it.unibo.db;

import java.time.LocalDate;
import java.util.Objects;

/**
 * An implementation of the {@link User} interface.
 */
public class UserImpl implements User {
    private final int userId;
    private final boolean receivedFirstAlert;
    private final boolean receivedSecondAlert;
    private final boolean receivedThirdAlert;
    private final LocalDate medicationAlertDate;

    /**
     * This constructor takes the user id as intended by this application, whether or not this user has received the first, the
     * second and the third of the three alerts about the last time they measured their blood pressure and the date in which this
     * user has received the last alert about the medication they should be taking.
     * @param userId the user id as intended by this application
     * @param receivedFirstAlert whether or not this user has received the first of the three alerts about the last time they
     * measured their blood pressure
     * @param receivedSecondAlert whether or not this user has received the second of the three alerts about the last time they
     * measured their blood pressure
     * @param receivedThirdAlert whether or not this user has received the third of the three alerts about the last time they
     * measured their blood pressure
     * @param medicationAlertDate the date in which this user has received the last alert about the medication they should be
     * taking
     */
    public UserImpl(final int userId,
                    final boolean receivedFirstAlert,
                    final boolean receivedSecondAlert,
                    final boolean receivedThirdAlert,
                    final LocalDate medicationAlertDate) {
        this.userId = userId;
        this.receivedFirstAlert = receivedFirstAlert;
        this.receivedSecondAlert = receivedSecondAlert;
        this.receivedThirdAlert = receivedThirdAlert;
        this.medicationAlertDate = Objects.requireNonNull(medicationAlertDate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getId() {
        return this.userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasReceivedFirstAlert() {
        return this.receivedFirstAlert;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasReceivedSecondAlert() {
        return this.receivedSecondAlert;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasReceivedThirdAlert() {
        return this.receivedThirdAlert;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDate getMedicationAlertDate() {
        return this.medicationAlertDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.receivedFirstAlert,
                            this.receivedSecondAlert,
                            this.receivedThirdAlert,
                            this.medicationAlertDate,
                            this.userId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserImpl)) {
            return false;
        }
        final var other = (UserImpl) obj;
        return this.receivedFirstAlert == other.receivedFirstAlert
               && this.receivedSecondAlert == other.receivedSecondAlert
               && this.receivedThirdAlert == other.receivedThirdAlert
               && this.medicationAlertDate.equals(other.medicationAlertDate)
               && this.userId == other.userId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "UserImpl [userId=" + this.userId + ", receivedFirstAlert=" + this.receivedFirstAlert + ", receivedSecondAlert="
               + this.receivedSecondAlert + ", receivedThirdAlert=" + this.receivedThirdAlert + ", medicationAlertDate="
               + this.medicationAlertDate + "]";
    }
}
