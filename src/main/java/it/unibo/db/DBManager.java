package it.unibo.db;

import com.pengrad.telegrambot.model.Message;
import org.apache.commons.lang3.tuple.Pair;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * The application component responsible for the interactions with the database, which allows to execute all possible queries
 * on the data previously acquired by the application itself.
 */
public interface DBManager {
    /**
     * It inserts a new user entity into the database with all its attributes set to their default values.
     * @param message the message with starts registration
     */
    void persistUser(Message message) throws SQLException;

    /**
     * Checks if a user is present in the database.
     * @param userId the id of the user as intended by this application
     */
    boolean checkUser(int userId) throws SQLException;

    /**
     * It inserts a new measurement entity into the database extracting all necessary attributes from the given {@link Measurement}
     * object. 
     * @param measurement the object representing the {@link Measurement} to persist
     */
    void persistUserMeasurement(Measurement measurement) throws SQLException;

    /**
     * It gets the idUser given the idTelegram of the current user using the chatbot.
     * @param idTelegram the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getIdUser(final int idTelegram, final Consumer<Optional<String>> handler) throws SQLException;

    /**
     * It generates a link code given the idTelegram of the current user.
     * @param idTelegram the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the link code generation in the database
     */
    void generateLinkCode(final int idTelegram, final Consumer<Optional<String>> handler) throws SQLException;

    /**
     * It gets the pressure and heart rate mean for the user which id is given and, when the fetch of the required data is
     * completed, it will be created a new {@link UndatedMeasurement} object from the retrieved information and it will be
     * called the given handler passing the created object. If an error occurs, it will be passed an {@link Optional#empty()}
     * instead of an {@link Optional} containing the created object.
     * @param idTelegram the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     * @param startTime the start date-time of the data to fetch from the database
     * @param endTime the end date-time of the data to fetch from the database
     */
    void getUserMean(final int idTelegram, final Consumer<Optional<? extends UndatedMeasurement>> handler, final LocalDateTime startTime, final LocalDateTime endTime) throws SQLException;

    /**
     * It gets the pressure and heart rate daily mean for the user which id is given and, when the fetch of the required data is
     * completed, it will be created a new {@link UndatedMeasurement} object from the retrieved information and it will be
     * called the given handler passing the created object. If an error occurs, it will be passed an {@link Optional#empty()}
     * instead of an {@link Optional} containing the created object.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getDailyMean(int userId, Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException;

    /**
     * It gets the pressure and heart rate weekly mean for the user which id is given and, when the fetch of the required data is
     * completed, it will be created a new {@link UndatedMeasurement} object from the retrieved information and it will be
     * called the given handler passing the created object. If an error occurs, it will be passed an {@link Optional#empty()}
     * instead of an {@link Optional} containing the created object.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getWeeklyMean(int userId, Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException;

    /**
     * It gets the pressure and heart rate monthly mean for the user which id is given and, when the fetch of the required data is
     * completed, it will be created a new {@link UndatedMeasurement} object from the retrieved information and it will be
     * called the given handler passing the created object. If an error occurs, it will be passed an {@link Optional#empty()}
     * instead of an {@link Optional} containing the created object.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getMonthlyMean(int userId, Consumer<Optional<? extends UndatedMeasurement>> handler) throws SQLException;

    /**
     * It gets previously inserted {@link Measurement}s of a time period specified associated to the user which id is given and, when the fetch of the
     * required data is completed, it will be created a new list of {@link Measurement} objects from the retrieved information, and it will be called
     * the given handler passing the created object. If an error occurs, it will be passed an empty list to the handler.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     * @param startTime the start date-time of the data to fetch from the database
     * @param endTime the end date-time of the data to fetch from the database
     */
    void getUserMeasurements(int userId, Consumer<List<? extends Measurement>> handler, LocalDateTime startTime, LocalDateTime endTime) throws SQLException;

    /**
     * It gets all previously inserted {@link Measurement}s associated to the user which id is given and, when the fetch of the
     * required data is completed, it will be created a new list of {@link Measurement} objects from the retrieved information
     * and it will be called the given handler passing the created object. If an error occurs, it will be passed an empty list
     * to the handler.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getAllUserMeasurements(int userId, Consumer<List<? extends Measurement>> handler) throws SQLException;

    /**
     * It gets previously inserted {@link Measurement}s of one day associated to the user which id is given and, when the fetch of the
     * required data is completed, it will be created a new list of {@link Measurement} objects from the retrieved information
     * and it will be called the given handler passing the created object. If an error occurs, it will be passed an empty list
     * to the handler.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     * @param startTime the start date-time of the data to fetch from the database
     * @param endTime the end date-time of the data to fetch from the database
     */
    void getDayUserMeasurements(int userId, Consumer<List<? extends Measurement>> handler, LocalDateTime startTime, LocalDateTime endTime) throws SQLException;

    /**
     * It gets the timestamp of the last inserted measurement for each user in the database and, when the fetch of the required
     * data is completed, it will be created a new list of {@link Pair} objects containing the timestamp of a last measurement
     * and the {@link User} which made it from the retrieved information and it will be called the given handler passing the
     * created object. If an error occurs, it will be passed an empty list to the handler.
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getLastMeasurementDates(Consumer<List<? extends Pair<? extends User, LocalDateTime>>> handler) throws SQLException;

    /**
     * It gets the timestamp of the last inserted measurement and all user data for the user which id is given and, when the fetch
     * of the required data is completed, it will be created a new {@link Pair} object containing the timestamp of the last
     * measurement and the associated user {@link User} object from the retrieved information and it will be called the given
     * handler passing the created object. If an error occurs, it will be passed an {@link Optional#empty()} instead of an
     * {@link Optional} containing the created object.
     * @param userId the id of the user as intended by this application
     * @param handler the handler that will be called upon completion of the data fetch from the database
     */
    void getLastUserMeasurement(int userId, Consumer<Optional<? extends Pair<? extends User, LocalDateTime>>> handler) throws SQLException;

    /**
     * It updates the user data into the database which concerns the alerts given after a certain time from the last pressure
     * measurement registered for the user which id is given.
     * @param userId the id of the user as intended by this application
     * @param firstAlertGiven if the user has now received the first alert
     * @param secondAlertGiven if the user has received the second alert
     * @param thirdAlertGiven if the user has received the third alert
     */
    void updateUserMeasurementAlerts(int userId, boolean firstAlertGiven, boolean secondAlertGiven, boolean thirdAlertGiven) throws SQLException;

    /**
     * It updates the user data into the database which concerns the medication alert by renewing the date in which has received
     * the last alert for the user which id is given.
     * @param userId the id of the user as intended by this application
     */
    void renewUserMedicationAlerts(int userId) throws SQLException;
}
