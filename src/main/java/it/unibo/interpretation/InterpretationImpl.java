package it.unibo.interpretation;

import io.vertx.core.json.JsonObject;
import java.util.*;

/**
 * An implementation of the {@link Interpretation} interface.
 */
public class InterpretationImpl implements Interpretation {
    private final InterpretationType type;
    private HashMap<String, Optional<?>> parameters = new HashMap<>();

    /**
     * This constructor takes the {@link InterpretationType} of this interpretation, an optional systolic pressure value contained
     * in this interpretation, an optional diastolic pressure value contained in it and an optional heart rate value contained in
     * it.
     * @param type the {@link InterpretationType} of this interpretation
     */
    protected InterpretationImpl(final InterpretationType type) {
        this.type = Objects.requireNonNull(type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InterpretationType getType() {
        return this.type;
    }

    @Override
    public HashMap<String, Optional<?>> getParameters() {
        return this.parameters;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getSystolicPressure() {
        return (Optional<Integer>) parameters.get("systolic");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getDiastolicPressure() {
        return (Optional<Integer>) parameters.get("diastolic");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> getHeartRate() {
        return (Optional<Integer>) parameters.get("heartRate");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.type);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InterpretationImpl)) {
            return false;
        }
        final var other = (InterpretationImpl) obj;
        return this.type == other.type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "InterpretationImpl [type=" + this.type + " parameters=" + this.parameters.toString() + "]";
    }

    /**
     * The builder for creating new instances of {@link InterpretationImpl}.
     */
    public static class Builder {
        private static final String INTENTS_KEY = "intents";
        private static final String INTENT_NAME_KEY = "name";
        private static final String ALREADY_BUILT = "This builder has already been used, it can not be reused";

        private InterpretationType type;
        private boolean built;

        /**
         * The default constructor, it starts the creation of a new instance of {@link InterpretationImpl} with an empty
         * {@link Interpretation}, one with {@link InterpretationType#NONE} and no value for systolic pressure, diastolic
         * pressure or heart rate.
         */
        public Builder() {
            this.type = InterpretationType.NONE;
            this.built = false;
        }

        /**
         * This constructor takes a {@link JsonObject} formatted according to the Wit.ai API and it starts the building of a new
         * object with the properties extracted from the given {@link JsonObject}.
         * @param json the {@link JsonObject} from which extracting the properties for building a new instance of
         * {@link InterpretationImpl}
         */
        public Builder(final JsonObject json) {
            this();
            Optional.ofNullable(json)
                    .map(j -> j.getJsonArray(INTENTS_KEY))
                    .filter(i -> i.size() > 0)
                    .map(i -> i.getJsonObject(0))
                    .ifPresent(i -> {
                        this.type = List.of(InterpretationType.values())
                                        .parallelStream()
                                        .filter(u -> u.getIntentName().equals(i.getString(INTENT_NAME_KEY)))
                                        .findFirst()
                                        .orElse(InterpretationType.NONE);
                    });
        }

        /**
         * It sets the {@link InterpretationType} for the instance to build.
         * @param type the {@link InterpretationType} for the instance to build to set
         * @return this {@link Builder} instance
         */
        public Builder setType(final InterpretationType type) {
            this.type = Objects.requireNonNull(type);
            return this;
        }

        /**
         * It builds a new instance of {@link InterpretationImpl}.
         * @return the newly created {@link InterpretationImpl} instance
         */
        public InterpretationImpl build() {
            if (this.built) {
                throw new IllegalStateException(ALREADY_BUILT);
            }
            this.built = true;
            return new InterpretationImpl(this.type);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            return Objects.hash(this.type);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Builder)) {
                return false;
            }
            final Builder other = (Builder) obj;
            return this.type == other.type;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString() {
            return "Builder [type=" + this.type + "]";
        }

    }
}
