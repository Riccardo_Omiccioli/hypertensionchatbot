package it.unibo.interpretation;

public class InterpretationParameter<T> {
    private T object;

    public InterpretationParameter(T object) {
        this.object = object;
    }

    public T getObject() {
        return this.object;
    }
}
