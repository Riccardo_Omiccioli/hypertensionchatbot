package it.unibo.interpretation;

import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import it.unibo.bot.UserMessage;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * An {@link Interpreter} for the content of texts.
 */
public class NaturalLanguageInterpreter implements Interpreter {
    private static final String API_URL = "api.wit.ai";
    private static final String API_ENDPOINT = "/message";
    private static final int API_PORT = 443;
    private static final String OAUTH_ROLE = "Bearer ";
    private static final String VERSION_KEY_NAME = "v";
    private static final String WIT_VERSION = "20200902";
    private static final String QUERY_KEY_NAME = "q";

    private final HttpRequest<JsonObject> request;

    /**
     * This constructor takes a {@link WebClient} objects so as to access the web and make requests to online services and the
     * token for the Wit.ai natural language classification service in order to being able to connect to it.
     * @param webClient a {@link WebClient} objects so as to access the web and make requests to online services
     * @param witToken the token for the Wit.ai natural language classification service in order to being able to connect to it
     */
    public NaturalLanguageInterpreter(final WebClient webClient, final String witToken) {
        this.request = webClient.get(API_PORT, API_URL, API_ENDPOINT)
                                .ssl(true)
                                .putHeader(HttpHeaders.AUTHORIZATION.toString(), OAUTH_ROLE + Objects.requireNonNull(witToken))
                                .addQueryParam(VERSION_KEY_NAME, WIT_VERSION)
                                .as(BodyCodec.jsonObject());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void interpret(final UserMessage message, final Consumer<Interpretation> handler) {
        message.getText().ifPresentOrElse(
            t -> this.request.addQueryParam(QUERY_KEY_NAME, t)
                    .send(a -> handler.accept(new InterpretationImpl.Builder(a.map(r -> r.body())
                            .otherwiseEmpty()
                            .result())
                            .build())),
            () -> handler.accept(new InterpretationImpl.Builder().build()));
    }
}
