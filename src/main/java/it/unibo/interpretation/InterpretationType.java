package it.unibo.interpretation;

/**
 * A possible type of {@link Interpretation} as extracted from as user message.
 */
public enum InterpretationType {
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting the idUser.
     */
    GET_ID("get_id"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for generating a link code.
     */
    GENERATE_LINK_CODE("generate_link_code"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting more knowledge on how to measure
     * correctly its own pressure.
     */
    PRESSURE_INFO("pressure_info"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting the mean pressure between two dates.
     */
    PRESSURE_MEAN_BETWEEN("pressure_mean_between"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting the monthly mean pressure.
     */
    PRESSURE_MEAN_MONTH("pressure_mean_month"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting the weekly mean pressure.
     */
    PRESSURE_MEAN_WEEK("pressure_mean_week"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting the daily mean pressure.
     */
    PRESSURE_MEAN_DAY("pressure_mean_day"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for messages which contains a measurement of the
     * user own pressure.
     */
    PRESSURE_SENT("pressure_post"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements.
     */
    PRESSURE_GET_ALL("pressure_get_all"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements between two dates.
     */
    PRESSURE_GET_BETWEEN("pressure_get_between"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements of one day.
     */
    PRESSURE_GET_DAY("pressure_get_day"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements of the past day.
     */
    PRESSURE_GET_PAST_DAY("pressure_get_past_day"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements of the past week.
     */
    PRESSURE_GET_PAST_WEEK("pressure_get_past_week"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for getting all user pressure measurements of the past month.
     */
    PRESSURE_GET_PAST_MONTH("pressure_get_past_month"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for messages of agreement.
     */
    AGREEMENT("agree"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message for messages of disagreement.
     */
    DISAGREEMENT("disagree"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message to reply when the users thanks the bot.
     */
    THANKS("thanks"),
    /**
     * It represents a type of {@link Interpretation} of the content of a message to reply when the users greets the bot.
     */
    HELLO("hello"),
    /**
     * It represents the null type of {@link Interpretation} of the content of a message.
     */
    NONE("");

    private final String intentName;

    /*
     * This constructor takes the name of the corresponding intent associated with this interpretation type.
     */
    InterpretationType(final String intentName) {
        this.intentName = intentName;
    }

    /**
     * It returns the name of the intent associated with this interpretation type.
     * @return the name of the intent associated with this interpretation type
     */
    public String getIntentName() {
        return this.intentName;
    }
}
