package it.unibo.interpretation;

import java.util.HashMap;
import java.util.Optional;

/**
 * An entity representing the interpretation of the content of a user message. It has a type and optionally some values that can
 * be extracted from the interpretation itself.
 */
public interface Interpretation {
    /**
     * It returns the {@link InterpretationType} of this interpretation.
     * @return the {@link InterpretationType} of this interpretation
     */
    InterpretationType getType();

    /**
     * It returns the parameter HashMap.
     * @return parameter HashMap
     */
    HashMap<String, Optional<?>> getParameters();

    /**
     * It returns a systolic pressure value, if present into the content of the message.
     * @return an {@link Optional} containing a systolic pressure value, if present into the content of the message, an
     * {@link Optional#empty()} otherwise
     */
    Optional<Integer> getSystolicPressure();

    /**
     * It returns a diastolic pressure value, if present into the content of the message.
     * @return an {@link Optional} containing a diastolic pressure value, if present into the content of the message, an
     * {@link Optional#empty()} otherwise
     */
    Optional<Integer> getDiastolicPressure();

    /**
     * It returns a heart rate value, if present into the content of the message.
     * @return an {@link Optional} containing a heart rate value, if present into the content of the message, an
     * {@link Optional#empty()} otherwise
     */
    Optional<Integer> getHeartRate();

}
