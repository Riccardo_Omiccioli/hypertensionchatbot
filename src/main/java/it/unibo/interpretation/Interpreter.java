package it.unibo.interpretation;

import it.unibo.bot.UserMessage;

import java.util.function.Consumer;

/**
 * An entity capable of interpreting asynchronously the content of a given message and produce an {@link Interpretation} for it.
 */
public interface Interpreter {
    /**
     * It interprets asynchronously the given message and, when the {@link Interpretation} is available, it will call the handler
     * for handling the result of its interpretation.
     * @param message the message to interpret
     * @param handler the callback to call when the interpretation has been completed
     */
    void interpret(UserMessage message, Consumer<Interpretation> handler);
}
