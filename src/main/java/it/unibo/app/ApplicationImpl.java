package it.unibo.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.DecodeException;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.web.client.WebClient;
import it.unibo.bot.MessageDispatcherImpl;
import it.unibo.db.DBManagerImpl;
import it.unibo.interpretation.NaturalLanguageInterpreter;

import java.io.IOException;
import java.util.Objects;

/**
 * The implementation of the {@link Application} interface.
 */
final class ApplicationImpl extends AbstractVerticle implements Application {
    private static final int SERVER_PORT = 8443;
    private static final String CONFIGURATION_FILE_PATH = "config.json";
    private static final long DAYS_TO_MILLISECONDS = 24 * 60 * 60 * 1000;
    private static final long VERTX_THREAD_BLOCKED_TIME = 5000;

    private final Configuration configuration;

    /**
     * This constructor takes the configuration parameters as an object of type {@link ConfigurationImpl}.
     * @param configuration the object containing the values for the configuration parameters
     */
    private ApplicationImpl(final Configuration configuration) {
        super();
        this.configuration = Objects.requireNonNull(configuration);
    }

    /**
     * The main method for launching this application.
     * @param args unused
     * @throws DecodeException if the configuration parameters stored in JSON format could not be correctly decoded from the file
     * which holds them
     * @throws IOException if an I/O error occurs while reading the configuration file
     */
    public static void main(final String[] args) throws DecodeException, IOException {
        String json = new String(ClassLoader.getSystemResource(CONFIGURATION_FILE_PATH).openStream().readAllBytes());
        Vertx.vertx(new VertxOptions().setWarningExceptionTime(VERTX_THREAD_BLOCKED_TIME));
        Vertx.vertx().deployVerticle(new ApplicationImpl(new ObjectMapper().readValue(json, ConfigurationImpl.class)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() throws Exception {
        final var dispatcher = new MessageDispatcherImpl(
            this,
            new NaturalLanguageInterpreter(WebClient.create(this.getVertx()),
                                           this.configuration.getWitAIToken()),
                                           new DBManagerImpl(this.configuration.getDatabasePort(),
                                                             this.configuration.getDatabaseHost(),
                                                             this.configuration.getDatabaseName(),
                                                             this.configuration.getDatabaseUser(),
                                                             this.configuration.getDatabasePassword()),
                                                             this.configuration.getTelegramBotToken());
        this.getVertx().createHttpServer(new HttpServerOptions().setSsl(true)
                .setHost("0.0.0.0")
                .setKeyCertOptions(new PemKeyCertOptions()
                        .setKeyPath(this.configuration.getKeyPath())
                        .setCertPath(this.configuration.getCertificatePath())))
                .requestHandler(r -> r.bodyHandler(b -> {
                    r.response().end();
                    dispatcher.processMessage(b.toString());
                }))
                .listen(SERVER_PORT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startDailyTimer(final Runnable handler) {
        this.getVertx().setPeriodic(DAYS_TO_MILLISECONDS, i -> handler.run());
    }
}
