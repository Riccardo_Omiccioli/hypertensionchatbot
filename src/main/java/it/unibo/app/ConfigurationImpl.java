package it.unibo.app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * An implementation of the {@link Configuration} interface.
 */
class ConfigurationImpl implements Configuration {
    private final String keyPath;
    private final String certificatePath;
    private final int databasePort;
    private final String databaseHost;
    private final String databaseName;
    private final String databaseUser;
    private final String databasePassword;
    private final String telegramBotToken;
    private final String witAIToken;

    /**
     * This constructor takes the path to the file containing the private key for the HTTPS protocol, the path to the file
     * containing the certificate for the HTTPS protocol, the port of the host which contains the database behind which the
     * database is located, the host which contains the database used by this application, the name of the database used by this
     * application, the name of the user on behalf of which this application is accessing the database, the password for the user
     * on behalf of which this application is accessing the database, the token used for the Telegram bot which makes the
     * front-end of this application and the token used for the Wit.ai service for natural language classification and
     * interpretation.
     * @param keyPath the path to the file containing the private key for the HTTPS protocol
     * @param certificatePath the path to the file containing the certificate for the HTTPS protocol
     * @param databasePort the port of the host which contains the database behind which the database is located
     * @param databaseHost the host which contains the database used by this application
     * @param databaseName the name of the database used by this application
     * @param databaseUser the name of the user on behalf of which this application is accessing the database
     * @param databasePassword the password for the user on behalf of which this application is accessing the database
     * @param telegramBotToken the token used for the Telegram bot which makes the front-end of this application
     * @param witAIToken the token used for the Wit.ai service for natural language classification and interpretation
     */
    @JsonCreator
    protected ConfigurationImpl(@JsonProperty("key_path") final String keyPath,
                                @JsonProperty("cert_path") final String certificatePath,
                                @JsonProperty("db_port") final int databasePort,
                                @JsonProperty("db_host") final String databaseHost,
                                @JsonProperty("db_name") final String databaseName,
                                @JsonProperty("db_user") final String databaseUser,
                                @JsonProperty("db_password") final String databasePassword,
                                @JsonProperty("bot_token") final String telegramBotToken,
                                @JsonProperty("wit_token") final String witAIToken) {
        this.keyPath = Objects.requireNonNull(keyPath);
        this.certificatePath = Objects.requireNonNull(certificatePath);
        this.databasePort = databasePort;
        this.databaseHost = Objects.requireNonNull(databaseHost);
        this.databaseName = Objects.requireNonNull(databaseName);
        this.databaseUser = Objects.requireNonNull(databaseUser);
        this.databasePassword = Objects.requireNonNull(databasePassword);
        this.telegramBotToken = Objects.requireNonNull(telegramBotToken);
        this.witAIToken = Objects.requireNonNull(witAIToken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getKeyPath() {
        return this.keyPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCertificatePath() {
        return this.certificatePath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDatabasePort() {
        return this.databasePort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDatabaseHost() {
        return this.databaseHost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDatabaseName() {
        return this.databaseName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDatabaseUser() {
        return this.databaseUser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDatabasePassword() {
        return this.databasePassword;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTelegramBotToken() {
        return this.telegramBotToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWitAIToken() {
        return this.witAIToken;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.certificatePath,
                            this.databaseHost,
                            this.databaseName,
                            this.databasePassword,
                            this.databasePort,
                            this.databaseUser,
                            this.keyPath,
                            this.telegramBotToken,
                            this.witAIToken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConfigurationImpl)) {
            return false;
        }
        final var other = (ConfigurationImpl) obj;
        return this.certificatePath.equals(other.certificatePath)
               && this.databaseHost.equals(other.databaseHost)
               && this.databaseName.equals(other.databaseName)
               && this.databasePassword.equals(other.databasePassword)
               && this.databasePort == other.databasePort
               && this.databaseUser.equals(other.databaseUser)
               && this.keyPath.equals(other.keyPath)
               && this.telegramBotToken.equals(other.telegramBotToken)
               && this.witAIToken.equals(other.witAIToken);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "ConfigurationImpl [keyPath=" + this.keyPath + ", certificatePath=" + this.certificatePath + ", databasePort="
               + this.databasePort + ", databaseHost=" + this.databaseHost + ", databaseName=" + this.databaseName
               + ", databaseUser=" + this.databaseUser + ", databasePassword=" + this.databasePassword + ", telegramBotToken="
               + this.telegramBotToken + ", witAIToken=" + this.witAIToken + "]";
    }
}
