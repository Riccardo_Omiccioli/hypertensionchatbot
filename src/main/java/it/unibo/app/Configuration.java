package it.unibo.app;

/**
 * A configuration object, an object capable of storing and returning values for all configuration parameters.
 */
interface Configuration {
    /**
     * It returns the path to the file containing the private key for the HTTPS protocol.
     * @return the path to the file containing the private key for the HTTPS protocol
     */
    String getKeyPath();

    /**
     * It returns the path to the file containing the certificate for the HTTPS protocol.
     * @return the path to the file containing the certificate for the HTTPS protocol
     */
    String getCertificatePath();

    /**
     * It returns the port of the host which contains the database behind which the database is located.
     * @return the port of the host which contains the database behind which the database is located
     */
    int getDatabasePort();

    /**
     * It returns the host which contains the database used by this application.
     * @return the host which contains the database used by this application
     */
    String getDatabaseHost();

    /**
     * It returns the name of the database used by this application.
     * @return the name of the database used by this application
     */
    String getDatabaseName();

    /**
     * It returns the name of the user on behalf of which this application is accessing the database.
     * @return the name of the user on behalf of which this application is accessing the database
     */
    String getDatabaseUser();

    /**
     * It returns the password for the user on behalf of which this application is accessing the database.
     * @return the password for the user on behalf of which this application is accessing the database
     */
    String getDatabasePassword();

    /**
     * It returns the token used for the Telegram bot which makes the front-end of this application.
     * @return the token used for the Telegram bot which makes the front-end of this application
     */
    String getTelegramBotToken();

    /**
     * It returns the token used for the Wit.ai service for natural language classification and interpretation.
     * @return the token used for the Wit.ai service for natural language classification and interpretation
     */
    String getWitAIToken();
}
