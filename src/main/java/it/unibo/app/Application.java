package it.unibo.app;

/**
 * The class representing the application as a whole, which can put it into execution or it can put into execution the needed
 * tasks.
 */
public interface Application {
    /**
     * It starts a new timer for executing a periodic action every day.
     * @param handler the task which will be executed at the timer expiration
     */
    void startDailyTimer(Runnable handler);
}
