# -*- coding: utf-8 -*-

import random
import requests
import json

data = []

# Number of new data to insert
data_number = 10

# Add prefix to the training phrase
prefix = "inserisci "

# Fill separator_single with the string of choice if precise training is needed
separator_single = ""

# Use a default separator list or the separator_single defined before
separator_list = separator_single if separator_single != "" else [
    ' ', ',', ', ', ' , ', '.', ' . ', ' - ', '-', '_', ' _ ', '/', ' / ', '|', ' | ']

for separator in separator_list:
    for i in range(data_number):
        systolic_pressure = random.randint(1, 200)
        diastolic_pressure = random.randint(1, 200)
        data.append({"text": str(prefix) + str(systolic_pressure) + str(separator) + str(diastolic_pressure),
                     "entities": [
                         {
                             "entity": "wit$number:systolic_pressure",
                             "start": len(prefix),
                             "end": len(prefix) + len(str(systolic_pressure)),
                             "body": str(systolic_pressure),
                             "entities": []
                             },
                         {
                             "entity": "wit$number:diastolic_pressure",
                             "start": len(prefix) + len(str(systolic_pressure)) + len(separator),
                             "end": len(prefix) + len(str(systolic_pressure)) + len(separator) + len(str(diastolic_pressure)),
                             "body": str(diastolic_pressure),
                             "entities": []
                             }
                         ],
                     "traits": [],
                     "intent": "pressure_post"})

for item in data:
    print(str(item) + "\n")

jsonString = json.dumps(data)
# insert ?v=x
url = "https://api.wit.ai/utterances?v="
# insert Bearer code (Bearer XXXXX)
headers = {'Content-type': 'application/json', 'Authorization': 'Bearer '}
result = requests.post(url, data=jsonString, headers=headers)
print("Result: {}".format(result.status_code))