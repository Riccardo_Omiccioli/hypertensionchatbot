CREATE SCHEMA users;

CREATE TABLE users.users(
    id serial NOT NULL,
    telegram_id integer NOT NULL,
    first_alert_given boolean NOT NULL DEFAULT false,
    second_alert_given boolean NOT NULL DEFAULT false,
    third_alert_given boolean NOT NULL DEFAULT false,
    medication_alert_date date NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT telegram_id_unique UNIQUE (telegram_id)
);

CREATE TABLE users.measurements(
    id serial NOT NULL,
    systolic integer NOT NULL,
    diastolic integer NOT NULL,
    heart_rate integer,
    datetime timestamp NOT NULL,
    user_id integer NOT NULL,
    CONSTRAINT measurements_pkey PRIMARY KEY (id)
);

ALTER TABLE users.measurements ADD CONSTRAINT users_fkey
	FOREIGN KEY (user_id)
    REFERENCES users.users(id)
    ON UPDATE CASCADE
    ON DELETE SET NULL;