CREATE SCHEMA IF NOT EXISTS "users";

CREATE TABLE IF NOT EXISTS "users"."User" (
    "idUser" char(12) NOT NULL,
    "idTelegram" int NULL,
    "email" varchar(255) NULL,
    "password" varchar(255) NULL,
    "linkCode" varchar(255) NULL,
    PRIMARY KEY ("idUser"),
    CONSTRAINT "idTelegram_UNIQUE"
    UNIQUE ("idTelegram")
);

CREATE TABLE IF NOT EXISTS "users"."Doctor" (
    "idDoctor" int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    "User_idUser" char(12) NOT NULL,
    PRIMARY KEY ("idDoctor"),
    CONSTRAINT "fk_Doctor_User"
    FOREIGN KEY ("User_idUser")
    REFERENCES "users"."User" ("idUser") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "fk_Doctor_User_idx" ON "users"."Doctor"("User_idUser" ASC);

CREATE TABLE IF NOT EXISTS "users"."Alert" (
    "idAlert" int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    "firstAlertGiven" boolean NULL,
    "secondAlertGiven" boolean NULL,
    "thirdAlertGiven" boolean NULL,
    "medicationAlertDate" timestamp NULL,
    "User_idUser" char(12) NOT NULL,
    PRIMARY KEY ("idAlert"),
    CONSTRAINT "idAlert_UNIQUE"
    UNIQUE ("idAlert"),
    CONSTRAINT "fk_Alert_User1"
    FOREIGN KEY ("User_idUser")
    REFERENCES "users"."User" ("idUser") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "fk_Alert_User1_idx" ON "users"."Alert"("User_idUser" ASC);

CREATE TABLE IF NOT EXISTS "users"."Admin" (
    "idAdmin" int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    "User_idUser" char(12) NOT NULL,
    PRIMARY KEY ("idAdmin"),
    CONSTRAINT "fk_Admin_User1"
    FOREIGN KEY ("User_idUser")
    REFERENCES "users"."User" ("idUser") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "fk_Admin_User1_idx" ON "users"."Admin"("User_idUser" ASC);

CREATE TABLE IF NOT EXISTS "users"."Consent" (
    "idConsent" int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    "message" text NOT NULL,
    "date" timestamp NOT NULL,
    "User_idUser" char(12) NOT NULL,
    PRIMARY KEY ("idConsent"),
    CONSTRAINT "fk_consent_User1"
    FOREIGN KEY ("User_idUser")
    REFERENCES "users"."User" ("idUser") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "fk_consent_User1_idx" ON "users"."Consent"("User_idUser" ASC);

CREATE TABLE IF NOT EXISTS "users"."Measurement" (
    "idMeasurement" int GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    "systolic" int NOT NULL,
    "diastolic" int NOT NULL,
    "heartRate" int NULL,
    "dateTime" timestamp NOT NULL,
    "User_idUser" char(12) NOT NULL,
    PRIMARY KEY ("idMeasurement"),
    CONSTRAINT "fk_Measurement_User1"
    FOREIGN KEY ("User_idUser")
    REFERENCES "users"."User" ("idUser") ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE INDEX "fk_Measurement_User1_idx" ON "users"."Measurement"("User_idUser" ASC);
